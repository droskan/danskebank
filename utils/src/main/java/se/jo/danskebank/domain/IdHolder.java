package se.jo.danskebank.domain;

/***
 * Simple IdHolder interface.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
public interface IdHolder {
	
	/***
	 * Method to retrieve id.
	 * @return Id.
	 */
	long getId();
	
}
