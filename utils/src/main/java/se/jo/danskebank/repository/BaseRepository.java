package se.jo.danskebank.repository;

import se.jo.danskebank.domain.IdHolder;

/***
 * Interface for CRUD methods on repositories.
 * @author oscarlothman/jonathanvlajkovic
 *
 * @param <E>
 */
public interface BaseRepository <E extends IdHolder> {
	
	/***
	 * Create.
	 * @param entity
	 * @return persist.
	 */
	long persist(E entity);
	
	/***
	 * Delete.
	 * @param entity
	 */
	void remove(E entity);
	
	/***
	 * Retrieve.
	 * @param id
	 * @return E.
	 */
	E findById(long id);
	
	/***
	 * Update.
	 * @param entity
	 */
	void update(E entity);

}
