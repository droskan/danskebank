package se.jo.danskebank.repository.jpa;

import java.lang.reflect.ParameterizedType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import se.jo.danskebank.domain.IdHolder;
import se.jo.danskebank.repository.BaseRepository;

/***
 * Abstract class that handles the database queries. 
 * @author oscarlothman/jonathanvlajkovic
 *
 * @param <E>
 */
public abstract class JpaRepository <E extends IdHolder> implements BaseRepository<E> {

	protected Class<E> entityClass;

	@PersistenceContext
	protected EntityManager em;

	@SuppressWarnings("unchecked")
	public JpaRepository() {
		ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
		this.entityClass = (Class<E>) genericSuperclass.getActualTypeArguments()[0];
	}

	@Override
	public long persist(E entity) {
		em.persist(entity);
		return entity.getId();
	}

	@Override
	public void remove(E entity) {
		em.remove(entity);
	}

	@Override
	public E findById(long id) {
		return em.find(entityClass, id);
	}

	@Override
	public void update(E entity) {
		em.merge(entity);
	}

}

