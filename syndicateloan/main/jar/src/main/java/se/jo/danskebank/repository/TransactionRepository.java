package se.jo.danskebank.repository;

import java.util.List;

import javax.ejb.Local;

import se.jo.danskebank.domain.Transaction;

/***
 * Interface for TransactionRepository. Handles repository calls that is explicit for for transaction.
 * Extends BaseRepository for basic CRUD functionality.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
@Local
public interface TransactionRepository extends BaseRepository<Transaction>{
	
	/***
	 * Method to fetch all Transactions from the database.
	 * @return List of all Transactions.
	 */
	List<Transaction> getAllTransactions();
	
	/***
	 * Method to fetch Transactions for a specific Borrower from database.
	 * @param id of the transaction.
	 * @return List of Transactions for a specific Borrower.
	 */
	List<Transaction> getTransactionsWithBorrowerId(long id);

}
