package se.jo.danskebank.domain.constants;

import java.util.ArrayList;
import java.util.List;

/***
 * Constants for Month.
 * Month that the transaction was made.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
public enum Month {

	JAN("1"), FEB("2"), MAR("3"), APR("4"), MAY("5"), JUN("6"), JUL("7"), AUG("8"), SEP("9"), OCT("10"), NOV("11"), DEC("12");

	private String month;
	
	// GETTERS & SETTERS

	private Month(String month) {
		this.month = month;
	}

	public String getMonth() {
		return month;
	}
	
	/***
	 * Method for getting month niceName.
	 * @return List of months niceName.
	 */
	public static List<String> getNiceNames() {

		List<String> months = new ArrayList<String>();
		for(Month t : Month.values()) {
			months.add(t.getMonth());
		}
		return months;

	}

}
