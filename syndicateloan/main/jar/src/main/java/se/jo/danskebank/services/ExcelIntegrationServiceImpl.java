package se.jo.danskebank.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import se.jo.danskebank.domain.Borrower;
import se.jo.danskebank.domain.Price;
import se.jo.danskebank.domain.Transaction;

/***
 * Implementation for ExcelIntegrationService.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
@Stateless
public class ExcelIntegrationServiceImpl implements ExcelIntegrationService {

	@Inject
	private BorrowerService borrowerService;
	@Inject
	private TransactionService transactionService;
	@Inject
	private PriceService priceService;
	private Sheet sheet;

	@Override
	public void copyBorrowerFromExcel() {

		for(Row row: getSheet()) {

			if(row.getRowNum() == 0) {
				continue;
			}

			Borrower borrower = new Borrower();
			for(Cell cell: row){

				if(cell.getColumnIndex() == 3) {
					int i = (int) cell.getNumericCellValue();
					borrower.setDebtCategory(i);
				}
				if(cell.getColumnIndex() == 4) {
					borrower.setNationality(cell.getStringCellValue());
				}
				if(cell.getColumnIndex() == 5) {

					borrower.setName(makeItLowerCase(cell.getStringCellValue()));
				}
				if(cell.getColumnIndex() == 6) {
					borrower.setDbRating(cell.getStringCellValue());
				}
				if(cell.getColumnIndex() == 7) {
					borrower.setExternalRating(cell.getStringCellValue());
				}
				if(cell.getColumnIndex() == 19) {
					borrower.setSector(makeItLowerCase(cell.getStringCellValue()));
				}

			}
			getBorrowerService().createBorrower(borrower);
			getBorrowerService().removeNullBorrowers();
		}
		copyTransactionFromExcel();
	}

	@Override
	public void copyTransactionFromExcel() {

		for(Row row: getSheet()) {

			if(row.getRowNum() == 0) {
				continue;
			}

			Transaction transaction = new Transaction();
			Price price = new Price();
			for(Cell cell: row){

				if(cell.getColumnIndex() == 1) {
					int year =(int) cell.getNumericCellValue();
					transaction.setYear(year);
				}
				if(cell.getColumnIndex() == 2) {
					int month = (int)cell.getNumericCellValue();
					transaction.setMonth(month);
				}
				if(cell.getColumnIndex() == 5) {
					transaction.setBorrower(getBorrowerService().getBorrowerByName(cell.getStringCellValue()));
				}
				if(cell.getColumnIndex() == 8) {
					transaction.setType(makeItLowerCase(cell.getStringCellValue()));
				}
				if(cell.getColumnIndex() == 9) {
					transaction.setFacility(makeItLowerCase(cell.getStringCellValue()));
				}
				if(cell.getColumnIndex() == 10) {
					transaction.setStatus(makeItLowerCase(cell.getStringCellValue()));
				}
				if(cell.getColumnIndex() == 11) {
					transaction.setAmount((long)cell.getNumericCellValue());
				}
				if(cell.getColumnIndex() == 12) {
					transaction.setMaturity((int)cell.getNumericCellValue());
				}
				if(cell.getColumnIndex() == 13) {
					transaction.setExtensionOptions((int)cell.getNumericCellValue());
				}
				if(cell.getColumnIndex() == 14) {
					price.setUpFrontFee(cell.getNumericCellValue());
				}
				if(cell.getColumnIndex() == 15) {
					price.setMargin(cell.getNumericCellValue());
				}
				if(cell.getColumnIndex() == 16) {
					price.setUtilizationFee(cell.getNumericCellValue());
				}
				if(cell.getColumnIndex() == 17) {
					price.setFullyDrawnPrice(cell.getNumericCellValue());
				}
				if(cell.getColumnIndex() == 18) {
					price.setComments(makeItLowerCase(cell.getStringCellValue()));
				}

				price.setTransaction(transaction);
				transaction.setPrice(price);
				getTransactionService().createTransaction(transaction);
				getPriceService().createPrice(price);

			}

		}

	}

	@Override
	public void selectExcelFile(String filePath) {

		FileInputStream file;

			HSSFWorkbook workbook;
			try {
				file = new FileInputStream(new File(filePath));
				workbook = new HSSFWorkbook(file);
				HSSFSheet sheet = workbook.getSheetAt(0);
			} catch (IOException e) {
				e.printStackTrace();
			}

			setSheet(sheet);
			
	}
	
	/***
	 * Cell value to lower case.
	 * @param cellValue
	 * @return Cell value.
	 */
	public String makeItLowerCase(String cellValue) {

		char c = cellValue.charAt(0);
		String v = cellValue.substring(1, cellValue.length());
		v = v.toLowerCase();
		cellValue = c+v;
		cellValue = cellValue.trim();

		return cellValue;

	}
	
	//GETTERS & SETTERS

	public BorrowerService getBorrowerService() {
		return borrowerService;
	}

	public void setBorrowerService(BorrowerService borrowerService) {
		this.borrowerService = borrowerService;
	}

	public TransactionService getTransactionService() {
		return transactionService;
	}

	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}

	public PriceService getPriceService() {
		return priceService;
	}

	public void setPriceService(PriceService priceService) {
		this.priceService = priceService;
	}

	public Sheet getSheet() {
		return sheet;
	}

	public void setSheet(Sheet sheet) {
		this.sheet = sheet;
	}

}
