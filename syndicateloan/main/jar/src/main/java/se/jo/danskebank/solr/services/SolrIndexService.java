package se.jo.danskebank.solr.services;

import javax.ejb.Local;

import se.jo.danskebank.domain.Transaction;

/***
 * Interface for SolrIndexService.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
@Local
public interface SolrIndexService {
	
	/***
	 * Method for creating an index out of a Transaction.
	 * @param transaction
	 */
	void createSolrDocTransaction(Transaction transaction);
	
	/***
	 * Index all Transactions.
	 */
	void indexAllTransactions();

}
