package se.jo.danskebank.domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/*** 
 * Domain object for Transaction. Holds all the information of the loan except the price information. 
 * Has an coupling to borrower, there can never be an transaction without an borrower. An borrower can have many transactions. 
 * Has an coupling to price.
 * @author oscarlothman/jonathanvlajkovic
 */
@Entity
public class Transaction implements IdHolder {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long transactionId;
	private String type;
	private int year;
	private int month;
	private String facility;
	private String status;
	private long amount;
	private int maturity;
	private int extensionOptions;
	@ManyToOne
	@JoinColumn(name = "borrowerId")
	private Borrower borrower;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "priceId")
	private Price price;

	/***
	 * Empty constructor.
	 */
	public Transaction() {

	}

	/***
	 * Constructor.
	 * @param transactionId id of the transaction
	 * @param type Type Says if the loan is syndicated between many banks or if it is a bilateral loan
	 * @param facility Determines what kind of loan it is
	 * @param status Will tell from which stage of the syndication we got the price information.
	 * @param amount that the borrower asked for.
	 * @param maturity Maturity Number of years the loan is available for. Normally 3-5 years.
	 * @param extensionOptions Number of extensions If the borrower has the opportunity to extend the loan after it matures. Normally 1-2 years
	 * @param borrower
	 * @param year of the transaction
	 * @param month of the transaction.
	 */
	public Transaction(long transactionId, String type, String facility, String status,
			long amount, int maturity, int extensionOptions, Borrower borrower, int year, int month) {
		this.transactionId = transactionId;
		this.type = type;
		this.facility = facility;
		this.status = status;
		this.amount = amount;
		this.maturity = maturity;
		this.extensionOptions = extensionOptions;
		this.borrower = borrower;
		this.year = year;
		this.month = month;
	}
	
	// GETTERS & SETTERS

	public long getId() {
		return transactionId;
	}

	public void setId(long transactionId) {
		this.transactionId = transactionId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFacility() {
		return facility;
	}

	public void setFacility(String facility) {
		this.facility = facility;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public int getMaturity() {
		return maturity;
	}

	public void setMaturity(int maturity) {
		this.maturity = maturity;
	}

	public int getExtensionOptions() {
		return extensionOptions;
	}

	public void setExtensionOptions(int extensionOptions) {
		this.extensionOptions = extensionOptions;
	}

	public Borrower getBorrower() {
		return borrower;
	}

	public void setBorrower(Borrower borrower) {
		this.borrower = borrower;
	}

	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

}
