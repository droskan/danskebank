package se.jo.danskebank.services;

import java.util.List;

import javax.ejb.Local;

import se.jo.danskebank.domain.Price;

/***
 * Interface for PriceService.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
@Local
public interface PriceService {
	
	/***
	 * Method to retrieve a Price with id.
	 * @param id
	 * @return Price.
	 */
	Price getPrice(long id);
	
	/***
	 * Method for creating a Price.
	 * @param price
	 * @return Price.
	 */
	Price createPrice(Price price);
	
	/***
	 * Method for updating an existing Price.
	 * @param price
	 */
	void updatePrice(Price price);
	
	/***
	 * Method to retrieve all Prices.
	 * @return List of Prices.
	 */
	List<Price> getAllPrices();
	
	/***
	 * Method to delete an existing Price with id.
	 * @param id
	 */
	void deletePrice(long id);

}
