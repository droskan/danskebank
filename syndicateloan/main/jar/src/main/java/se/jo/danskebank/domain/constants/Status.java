package se.jo.danskebank.domain.constants;

import java.util.ArrayList;
import java.util.List;

/*** 
 * Constants for Status.
 * Will tell from which stage of the syndication we got the price information.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
public enum Status {

	INDICATION("Indication"), PRICE_VIEW("Price view"),
	IN_SYNDICATION("In syndication"),
	SIGNED("Signed"), SUSPENDED("Suspended");

	private String status;
	
	// GETTERS & SETTERS

	private Status(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}
	
	/***
	 * Method for getting status niceName.
	 * @return List of statuses niceName.
	 */
	public static List<String> getNiceNames() {

		List<String> statuses = new ArrayList<String>();
		for(Status t : Status.values()) {
			statuses.add(t.getStatus());
		}
		return statuses;

	}

}
