package se.jo.danskebank.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.jo.danskebank.domain.Price;
import se.jo.danskebank.repository.PriceRepository;

/***
 * Implementation for PriceService.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
@Stateless
public class PriceServiceImpl implements PriceService {

	@Inject
	PriceRepository repository;
	
	//CRUD
	
	@Override
	public Price getPrice(long id) {
		return getRepository().findById(id);
	}

	@Override
	public Price createPrice(Price price) {
		long id = getRepository().persist(price);
		return getRepository().findById(id);
	}

	@Override
	public void updatePrice(Price price) {
		getRepository().update(price);
	}

	@Override
	public List<Price> getAllPrices() {
		return getRepository().getAllPrices();
	}

	@Override
	public void deletePrice(long id) {
		getRepository().remove(getPrice(id));
	}
	
	//GETTERS & SETTERS

	public PriceRepository getRepository() {
		return repository;
	}

	public void setRepository(PriceRepository repository) {
		this.repository = repository;
	}

}
