package se.jo.danskebank.services;

import java.util.List;

import javax.ejb.Local;

import se.jo.danskebank.domain.Transaction;

/***
 * Interface for TransactionService.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
@Local
public interface TransactionService {
	
	/***
	 * Method to retrieve a Transaction with id.
	 * @param id
	 * @return Transaction.
	 */
	Transaction getTransaction(long id);
	
	/***
	 * Method for creating a Transaction.
	 * @param transaction
	 * @return Transaction.
	 */
	Transaction createTransaction(Transaction transaction);
	
	/***
	 * Method for updating an existing Transaction.
	 * @param transaction
	 */
	void updateTransaction(Transaction transaction);
	
	/***
	 * Method to retrieve all Transactions.
	 * @return List of Transaction.
	 */
	List<Transaction> getAllTransactions();
	
	/***
	 * Method to retrieve Transactions for a specific Borrower with id.
	 * @param borrowerId
	 * @return List of Transactions.
	 */
	List<Transaction> getTransactionsWithBorrowerId(long borrowerId);
	
	/***
	 * Method to delete an existing Transaction with id.
	 * @param id
	 */
	void deleteTransaction(long id);

}
