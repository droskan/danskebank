package se.jo.danskebank.domain.constants;

import java.util.ArrayList;
import java.util.List;

/*** 
 * Constants for SortFields.
 * Sort parameters for search result.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
public enum SortFields {
	
	NAME("name"), NATIONALITY("nationality"), SECTOR("sector"), DEBT_CATEGORY("debtCategory"), DB_RATING("dbRating"),
	EXTERNAL_RATING("externalRating"), MONTH("month"), YEAR("year"), AMOUNT("amount"), MATURITY("maturity"),
	EXTENSION_OPTIONS("extensionOptions"), TYPE("type"), FACILITY("facility"), STATUS("status"), UP_FRONT_FEE("upFrontFee"),
	MARGIN("margin"), UTILIZATION_FEE("utilizationFee"), FULLY_DRAWN_PRICE("fullyDrawnPrice");
	
	private String sortField;
	
	// GETTERS & SETTERS
	
	private SortFields(String sortFields) {
		this.sortField = sortFields;
	}

	public String getSortField() {
		return sortField;
	}
	
	/***
	 * Method for getting sortField niceName.
	 * @return List of sortFields niceName.
	 */
	public static List<String> getNiceNames() {

		List<String> sortFields = new ArrayList<String>();
		for(SortFields t : SortFields.values()) {
			sortFields.add(t.getSortField());
		}
		return sortFields;

	}

	
}
