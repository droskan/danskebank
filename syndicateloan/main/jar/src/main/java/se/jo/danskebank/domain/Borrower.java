package se.jo.danskebank.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.joda.time.DateTime;

/***
 * Domain object for Borrower. Holds all the information about the company.
 * Doesn't have any hard connections with the other domains so there can exist borrowers without any transactions.
 * Dosen't hold any values about loans or prices.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
@Entity
public class Borrower implements IdHolder {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long borrowerId;
	private String name;
	private String nationality;
	private String sector;
	private int debtCategory;
	private String dbRating;
	private String externalRating;
	
	/***
	 * Empty constructor.
	 */
	public Borrower() {

	}
	
	/***
	 * Constructor.
	 * @param borrowerId Id for the borrower.
	 * @param name of the company
	 * @param date when the borrower where created.
	 * @param nationality of the company
	 * @param sector the sector the company manly operates in.
	 * @param debtCategory the company's financing profile
	 * @param dbRating DB rating A rating of the credit quality of the company.
	 * @param externalRating Rating determined by Standard & Poor.
	 */
	public Borrower(long borrowerId, String name, DateTime date, String nationality, 
			String sector, int debtCategory, String dbRating, String externalRating) {
		setId(borrowerId);
		setName(name);
		setNationality(nationality);
		setSector(sector);
		setDebtCategory(debtCategory);
		setDbRating(dbRating);
		setExternalRating(externalRating);
	}
	
	// GETTERS & SETTERS

	public long getId() {
		return borrowerId;
	}

	public void setId(long borrowerId) {
		this.borrowerId = borrowerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public int getDebtCategory() {
		return debtCategory;
	}

	public void setDebtCategory(int debtCategory) {
		this.debtCategory = debtCategory;
	}

	public String getDbRating() {
		return dbRating;
	}

	public void setDbRating(String dbRating) {
		this.dbRating = dbRating;
	}

	public String getExternalRating() {
		return externalRating;
	}

	public void setExternalRating(String externalRating) {
		this.externalRating = externalRating;
	}

}
