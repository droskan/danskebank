package se.jo.danskebank.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.jo.danskebank.domain.Borrower;
import se.jo.danskebank.repository.BorrowerRepository;

/***
 * Implementation for BorrowerService.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
@Stateless
public class BorrowerServiceImpl implements BorrowerService {

	@Inject
	private BorrowerRepository repository;
	
	//CRUD

	@Override
	public Borrower getBorrower(long id) {
		return getRepository().findById(id);
	}

	@Override
	public Borrower createBorrower(Borrower borrower) {

		if(checkIfborrowerExists(borrower.getName())) {
			long id = getRepository().persist(borrower);
			return getRepository().findById(id);
		}

		return null;
	}

	@Override
	public void updateBorrower(Borrower borrower) {
		getRepository().update(borrower);
	}

	@Override
	public List<Borrower> getAllBorrowers() {
		return getRepository().getAllBorrowers();
	}

	@Override
	public void deleteBorrower(long id) {
		getRepository().remove(getBorrower(id));
	}

	@Override
	public Borrower getBorrowerByName(String name) {
		return getRepository().getBorrowerByName(name);
	}
	
	/***
	 * Check if the borrower with id exists.
	 * @param name of the borrower.
	 * @return true/false
	 */
	public boolean checkIfborrowerExists(String name) {

		if(getBorrowerByName(name) == null) {
			return true;
		}

		return false;
	}

	//Weird solution for removing null borrowers...
	public void removeNullBorrowers() {

		List<Borrower> borrowers = getAllBorrowers();
		for(Borrower b: borrowers) {
			if (b.getName() == null) {
				deleteBorrower(b.getId());
			}
		}

	}
	
	// GETTERS & SETTERS

	public BorrowerRepository getRepository() {
		return repository;
	}

	public void setRepository(BorrowerRepository repository) {
		this.repository = repository;
	}

}
