package se.jo.danskebank.repository;

import java.util.List;

import javax.ejb.Local;

import se.jo.danskebank.domain.Price;

/***
 * Interface for PriceRepository. Handles repository calls that is explicit for for price.
 * Extends BaseRepository for basic CRUD functionality.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
@Local
public interface PriceRepository extends BaseRepository<Price> {
	
	/***
	 * Method to fetch all Prices from the database.
	 * @return List of all Prices.
	 */
	List<Price> getAllPrices();

}
