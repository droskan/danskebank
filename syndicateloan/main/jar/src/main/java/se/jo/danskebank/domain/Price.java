package se.jo.danskebank.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/*** 
 * Domain object for Price. Holds all the price variables of an loan. Has an hard connection with the transaction domain.
 * Should not exist without a transaction. 
 * @author oscarlothman/jonathanvlajkovic
 */
@Entity
public class Price implements IdHolder {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long priceId;
	private double upFrontFee;
	private double margin;
	private double utilizationFee;
	private double fullyDrawnPrice;
	private String comments;
	@OneToOne(mappedBy = "price")
	private Transaction transaction;
	
	/***
	 * Empty constructor.
	 */
	public Price() {
		
	}
	
	/***
	 * Constructor.
	 * @param priceId id of the price
	 * @param upFrontFee The fee paid by the borrower when the documents are signed
	 * @param margin Margin The "price" of the loan
	 * @param utilizationFee A fee paid by the borrower if he/she "draws" the money.
	 * @param fullyDrawnPrice value of Margin + UtilzationFee
	 * @param comments Users own comments of the price.
	 * @param transaction
	 */
	public Price(long priceId, double upFrontFee, double margin,
			double utilizationFee, double fullyDrawnPrice, String comments,
			Transaction transaction) {
		this.priceId = priceId;
		this.upFrontFee = upFrontFee;
		this.margin = margin;
		this.utilizationFee = utilizationFee;
		this.fullyDrawnPrice = fullyDrawnPrice;
		this.comments = comments;
		this.transaction = transaction;
	}

	// GETTERS & SETTERS

	public long getId() {
		return priceId;
	}

	public void setId(long priceId) {
		this.priceId = priceId;
	}

	public double getUpFrontFee() {
		return upFrontFee;
	}

	public void setUpFrontFee(double upFrontFee) {
		this.upFrontFee = upFrontFee;
	}

	public double getMargin() {
		return margin;
	}

	public void setMargin(double margin) {
		this.margin = margin;
	}

	public double getUtilizationFee() {
		return utilizationFee;
	}

	public void setUtilizationFee(double utilizationFee) {
		this.utilizationFee = utilizationFee;
	}

	public double getFullyDrawnPrice() {
		return fullyDrawnPrice;
	}

	public void setFullyDrawnPrice(double fullyDrawnPrice) {
		this.fullyDrawnPrice = fullyDrawnPrice;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

}