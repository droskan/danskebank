package se.jo.danskebank.domain.constants;

import java.util.ArrayList;
import java.util.List;

/*** 
 * Constants for Facility.
 * Determines what kind of loan it is.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
public enum Facility {

	TL("TL"), RCF_BACK_STOP("RCF, back stop"), RCF_DRAWN("RCF, drawn"),
	GUARANTEE("Guarantee"), BRIDGE("Bridge"), OTHER("Other");

	private String facility;
	
	// GETTERS & SETTERS

	private Facility(String facility) {
		this.facility= facility;
	}

	public String getFacility() {
		return facility;
	}
	
	/***
	 * Method for getting facility niceName.
	 * @return List of facilities niceName.
	 */
	public static List<String> getNiceNames() {

		List<String> facilities = new ArrayList<String>();
		for(Facility t : Facility.values()) {
			facilities.add(t.getFacility());
		}
		return facilities;

	}

}
