package se.jo.danskebank.domain;

/***
 * Domain object for Loan. This is a domain object used for paring the other domains that belongs to each others.
 * Dosen't have an own table in the DB.
 * Mainly used for the Solr indexing and presentation of the data.
 * @author oscarlothman/jonathanvlajkovic
 */
public class Loan {

	private Borrower borrower;
	private Transaction transaction;
	private Price price;
	
	/***
	 * Empty constructor.
	 */
	public Loan() {

	}
	
	/***
	 * Constructor.
	 * @param borrower referent of the borrower domain.
	 * @param transaction referent of the transaction domain.
	 * @param price referent of the price domain.
	 */
	public Loan(Borrower borrower, Transaction transaction, Price price) {
		this.borrower = borrower;
		this.transaction = transaction;
		this.price = price;
	}

	// GETTERS & SETTERS
	
	public Borrower getBorrower() {
		return borrower;
	}
	public void setBorrower(Borrower borrower) {
		this.borrower = borrower;
	}
	public Transaction getTransaction() {
		return transaction;
	}
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}
	public Price getPrice() {
		return price;
	}
	public void setPrice(Price price) {
		this.price = price;
	}

}
