package se.jo.danskebank.repository.jpa;

import java.util.List;

import javax.persistence.Query;

import se.jo.danskebank.domain.Borrower;
import se.jo.danskebank.repository.BorrowerRepository;

/***
 * Implementation of BorrowerRepository with JPA.
 * Holds the database queries for borrower.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
public class JpaBorrowerRepository extends JpaRepository<Borrower> implements BorrowerRepository {
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Borrower> getAllBorrowers() {

		Query query = em.createQuery("select b from Borrower b");
		return query.getResultList();

	}

	@SuppressWarnings("unchecked")
	@Override
	public Borrower getBorrowerByName(String name) {

		Query query = em.createQuery("select b from Borrower b where b.name = ?1");
		query.setParameter(1, name);

		List<Borrower> resultList = query.getResultList();

		if (resultList.size() == 1) {
			return resultList.get(0);
		} else if (resultList.isEmpty()) {
			return null;
		} else {
			throw new RuntimeException("Recieved two borrowers for name " + name + ". This should not be possible.");
		}

	}

}
