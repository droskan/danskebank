package se.jo.danskebank.solr.services;

import java.util.List;

import javax.ejb.Local;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.common.SolrInputDocument;

/***
 * Interface for SolrServerService.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
@Local
public interface SolrServerService {
	
	/***
	 * Method to retrieve a SolrServer.
	 * @return SolrServer.
	 */
	SolrServer getSolrHTTPServer();
	
	/***
	 * Commit index.
	 * @param doc
	 */
	void commitDocToSolrServer(SolrInputDocument doc);
	
	/***
	 * Delete Solr document with id.
	 * @param id
	 */
	void deleteById(String id);
	
	/***
	 * Commit Solr documents.
	 * @param docs
	 * @return Number of documents.
	 */
	long commitDocumentsToSolr(List<SolrInputDocument> docs);
	
	/***
	 * Delete all Solr data.
	 */
	void deleteAllSolrData();

}
