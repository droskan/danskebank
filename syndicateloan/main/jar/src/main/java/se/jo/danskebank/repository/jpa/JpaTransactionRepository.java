package se.jo.danskebank.repository.jpa;

import java.util.List;

import javax.persistence.Query;

import se.jo.danskebank.domain.Transaction;
import se.jo.danskebank.repository.TransactionRepository;

/***
 * Implementation of TransactionRepository with JPA.
 * Holds the database queries for transaction.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
public class JpaTransactionRepository extends JpaRepository<Transaction> implements TransactionRepository {

	@SuppressWarnings("unchecked")
	@Override
	public List<Transaction> getAllTransactions() {

		Query query = em.createQuery("select t from Transaction t");
		return query.getResultList();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Transaction> getTransactionsWithBorrowerId(long id) {

		Query query = em.createQuery("select t from Transaction t where borrowerId =" + id);
		return query.getResultList();

	}

}
