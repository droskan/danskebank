package se.jo.danskebank.solr.services;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import se.jo.danskebank.domain.Borrower;
import se.jo.danskebank.domain.Loan;
import se.jo.danskebank.domain.Price;
import se.jo.danskebank.domain.Transaction;
import se.jo.danskebank.domain.constants.DomainType;
import se.jo.danskebank.services.TransactionService;

/***
 * Implementation for SolrHandleDataService.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
@Stateless
public class SolrHandleDataServiceImpl implements SolrHandleDataService{
	
	private Logger log = Logger.getLogger(SolrHandleDataServiceImpl.class.getName());

	@Inject
	private SolrServerService serverService;

	@Inject
	private TransactionService transactionService;

	@Inject
	private SolrIndexService solrIndexService;

	private QueryResponse queryResponse;
	private List<Loan> loans = new ArrayList<Loan>();

	@Override
	public QueryResponse generateQuery(List<String> queries, String sortField) {

		SolrQuery solrQuery = new SolrQuery();
		log.fine("queries size:" + queries.size());

		if(queries.contains("name")) {

			for(int i = 0; i <= queries.size() -1; i ++) {
				if (queries.get(i).contains("name")) {
					solrQuery.setQuery(queries.get(i));
					queries.remove(queries.get(i));
				}
			}

		} else {
			solrQuery.setQuery("*:*");
		}

		for(String q : queries) {
			solrQuery.addFilterQuery(q);
		}

		solrQuery.setFacet(true);
		solrQuery.setFacetMinCount(1);
		solrQuery.setFacetLimit(8);
		solrQuery.addFacetField("nationality");
		solrQuery.addFacetField("externalRating");
		solrQuery.addFacetField("type");
		solrQuery.setRows(1000);
		log.fine("sort: " + sortField);
		if(sortField == "") {
			sortField = "name";
		}
		solrQuery.addSort(sortField, SolrQuery.ORDER.asc);

		QueryResponse response = null;

		try {
			response = getServerService().getSolrHTTPServer().query(solrQuery);

		} catch (SolrServerException e) {
			log.info("Solr Exception: " + e);
			e.printStackTrace();
		}

		setQueryResponse(response);

		return response;

	}

	@Override
	public List<Loan> getSolrSearchResults(QueryResponse response) {

		SolrDocumentList docs = response.getResults();

		for(SolrDocument doc: docs) {

			//Borrower
			Borrower borrower = new Borrower();
			borrower.setName((String) doc.getFieldValue("name"));
			borrower.setId((Long) doc.getFieldValue("borrowerId"));
			borrower.setNationality((String) doc.getFieldValue("nationality"));
			borrower.setDbRating((String) doc.getFieldValue("dbRating"));
			borrower.setDebtCategory((Integer)doc.getFieldValue("debtCategory"));
			borrower.setExternalRating((String) doc.getFieldValue("externalRating"));
			borrower.setSector((String) doc.getFieldValue("sector"));

			//Transaction
			Transaction transaction = new Transaction();
			transaction.setAmount((Long) doc.getFieldValue("amount"));
			transaction.setBorrower(borrower);
			transaction.setExtensionOptions((Integer) doc.getFieldValue("extensionOptions"));
			transaction.setFacility((String) doc.getFieldValue("facility"));
			transaction.setId((Long) doc.getFieldValue("transactionId"));
			transaction.setMaturity((Integer) doc.getFieldValue("maturity"));
			transaction.setStatus((String) doc.getFieldValue("status"));
			transaction.setType((String) doc.getFieldValue("type"));
			transaction.setMonth((Integer) doc.getFieldValue("month"));
			transaction.setYear((Integer) doc.getFieldValue("year"));

			//Price
			Price price = new Price();
			price.setComments((String) doc.getFieldValue("comments"));
			price.setFullyDrawnPrice((Double) doc.getFieldValue("fullyDrawnPrice"));
			price.setId((Long) doc.getFieldValue("priceId"));
			price.setMargin((Double) doc.getFieldValue("margin"));
			price.setTransaction(transaction);
			price.setUpFrontFee((Double) doc.getFieldValue("upFrontFee"));
			price.setUtilizationFee((Double) doc.getFieldValue("utilizationFee"));

			transaction.setPrice(price);

			Loan loan = new Loan();
			loan.setBorrower(borrower);
			loan.setTransaction(transaction);
			loan.setPrice(price);

			getLoans().add(loan);

		}
		return getLoans();
	}

	@Override
	public void updateIndex(long transactionId) {

		SolrQuery query = new SolrQuery();
		query.setQuery("*:*");
		log.fine("transaction-id: " + transactionId);
		query.addFilterQuery("transactionId: " + transactionId);

		QueryResponse response = null;

		try {
			response = getServerService().getSolrHTTPServer().query(query);

		} catch (SolrServerException e) {
			log.info("Solr Exception: " + e);
			e.printStackTrace();
		}

		SolrDocumentList docs = response.getResults();
		String id = (String) docs.get(0).getFieldValue("id");

		getServerService().deleteById(id);
		getSolrIndexService().createSolrDocTransaction(getTransactionService().getTransaction(transactionId));

	}
	
	/***
	 * ???
	 * @return
	 */
	public List<FacetField.Count> getResult() {

		List<FacetField.Count> counts = new ArrayList<FacetField.Count>();
		QueryResponse result = getQueryResponse();
		List<FacetField> limitingFacets = result.getFacetFields();

		for (FacetField facet : limitingFacets) {

			if (facet == null) {
				continue;
			}

			List<Count> values = facet.getValues();
			if (values == null) {
				continue;
			}

			for (Count c : values) {
				if (c == null) {
					continue;
				}
				counts.add(c);
			}
		}
		return counts;
	}

	@Override
	public Transaction getTransaction(SolrDocument doc) {
		long id = (Long) doc.getFieldValue("transactionId");
		return getTransactionService().getTransaction(id);
	}

	
	@Override
	public SolrDocumentList getSolrDocsWithDomainId(long domainID, DomainType domainType) {
		
		SolrQuery query = new SolrQuery();
		query.setQuery("*:*");
		query.addFilterQuery(domainType.getDomainType() + ": " + domainID);

		QueryResponse response = null;

		try {
			response = getServerService().getSolrHTTPServer().query(query);

		} catch (SolrServerException e) {
			log.info("Solr Exception: " + e);
			e.printStackTrace();
		}

		SolrDocumentList docs = response.getResults();
	
		return docs;
		
		
	}
	
	// GETTERS & SETTERS
	
	public SolrServerService getServerService() {
		return serverService;
	}

	public void setServerService(SolrServerService serverService) {
		this.serverService = serverService;
	}

	public TransactionService getTransactionService() {
		return transactionService;
	}

	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}

	public SolrIndexService getSolrIndexService() {
		return solrIndexService;
	}

	public void setSolrIndexService(SolrIndexService solrIndexService) {
		this.solrIndexService = solrIndexService;
	}

	public QueryResponse getQueryResponse() {
		return queryResponse;
	}

	public void setQueryResponse(QueryResponse queryResponse) {
		this.queryResponse = queryResponse;
	}

	public List<Loan> getLoans() {
		return loans;
	}

	public void setLoans(List<Loan> loans) {
		this.loans = loans;
	}


}
