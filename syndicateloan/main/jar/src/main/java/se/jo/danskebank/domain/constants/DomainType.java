package se.jo.danskebank.domain.constants;

/*** 
 * Constants for DomainType.
 * Used for getting Solr docs from a specific type of domain.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
public enum DomainType {

	BORROWER_ID("borrowerId"), TRANSACTION_ID("transactionId"), PRICE_ID("priceId");
	
	private String domainType;
	
	// GETTERS & SETTERS
	
	private DomainType(String domainType) {
		this.domainType= domainType;
	}

	public String getDomainType() {
		return domainType;
	}
	
}
