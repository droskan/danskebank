package se.jo.danskebank.domain.constants;

/***
 * Constants for Nationality.
 * The borrowers nationality.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
public enum Nationality {

	SE("SE"), DK("DK"), NO("NO"), FI("FI"), AT("AT"), AU("AU"), BE("BE"),
	CA("CA"), CH("CH"), DE("DE"), ES("ES"), FR("FR"), IE("IE"), IT("IT"),
	LU("LU"), NL("NL"), UK("UK"), US("US"), OTHER("Other");

	private String nationality;
	
	// GETTERS & SETTERS

	private Nationality(String nationality) {
		this.nationality= nationality;
	}

	public String getNationality() {
		return nationality;
	}

}
