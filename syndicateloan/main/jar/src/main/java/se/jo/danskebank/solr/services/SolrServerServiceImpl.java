package se.jo.danskebank.solr.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.common.SolrInputDocument;

/***
 * Implementation for SolrServerService.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
@Stateless
public class SolrServerServiceImpl implements SolrServerService{
	
	private Logger log = Logger.getLogger(SolrServerServiceImpl.class.getName());

	private List<SolrInputDocument> solrDocs = new ArrayList<SolrInputDocument>();

	@Override
	public SolrServer getSolrHTTPServer() {
		SolrServer server = new HttpSolrServer("http://localhost:8080/solr");
		return server;
	}

	@Override
	public void commitDocToSolrServer(SolrInputDocument doc) {

		SolrServer server = getSolrHTTPServer();

		try {
			server.add(doc);
			server.commit();
		} catch (SolrServerException e) {
			log.info("Could not add to Solr: " + e);
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void deleteById(String id) {

		SolrServer server = getSolrHTTPServer();
		try {
			server.deleteById(id);
			server.commit();
		} catch (SolrServerException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public long commitDocumentsToSolr(List<SolrInputDocument> docs) {

		SolrServer server = getSolrHTTPServer();

		try {
			server.add(docs);
			server.commit();
		} catch (SolrServerException e) {
			log.info("Could not add to Solr: " + e);
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return docs.size();
	}

	@Override
	public void deleteAllSolrData() {
		SolrServer server = getSolrHTTPServer();
		try {
			server.deleteByQuery("*:*");
			server.commit();
		} catch (SolrServerException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	// GETTERS & SETTERS
	
	public List<SolrInputDocument> getSolrDocs() {
		return solrDocs;
	}

	public void setSolrDocs(List<SolrInputDocument> solrDocs) {
		this.solrDocs = solrDocs;
	}


}
