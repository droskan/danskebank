package se.jo.danskebank.services;

import java.util.List;

import javax.ejb.Local;

import se.jo.danskebank.domain.Borrower;

/***
 * Interface for BorrowerService. Basic CRUD functionality and other useful methods for borrowers. 
 * @author oscarlothman/jonathanvlajkovic
 *
 */
@Local
public interface BorrowerService {
	
	/***
	 * Method to retrieve a Borrower with id.
	 * @param id of the borrower
	 * @return Borrower.
	 */
	Borrower getBorrower(long id);
	
	/***
	 * Method for creating a borrower.
	 * @param borrower instance of borrower.
	 * @return Borrower.
	 */
	Borrower createBorrower(Borrower borrower);
	
	/***
	 * Method for updating an existing Borrower.
	 * @param borrower instance of an borrower
	 */
	void updateBorrower(Borrower borrower);
	
	/***
	 * Method to retrieve all Borrowers.
	 * @return List of Borrowers.
	 */
	List<Borrower> getAllBorrowers();
	
	/***
	 * Method to delete an existing Borrower with id.
	 * @param id of borrower.
	 */
	void deleteBorrower(long id);
	
	/***
	 * Method to retrieve a Borrower with a specific name.
	 * @param name of borrower
	 * @return Borrower.
	 */
	Borrower getBorrowerByName(String name);
	
	/***
	 * Method to delete your Borrowers with null values from DB.
	 */
	void removeNullBorrowers();

}
