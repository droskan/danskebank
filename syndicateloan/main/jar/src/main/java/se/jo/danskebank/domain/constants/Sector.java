package se.jo.danskebank.domain.constants;

import java.util.ArrayList;
import java.util.List;

/*** 
 * Constants for Sector.
 * Main occupation for the borrower.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
public enum Sector {

	COMMODITIES("Commodities"), CONSTRUCTION("Construction"), CONSUMER_DISCRETIONARY("Consumer Discretionary"), 
	CONSUMER_STAPLES("Consumer Staples"), FINANCIAL("Financial"), FOOD_AND_BEVERAGES("Food & Beverages"),
	HEALTH_CARE("Health Care"), HOLDING_COMPANY_CONGLOMERATE("Holding company/Conglomerate"), INDUSTRIAL("Industrial"),
	INSURANCE("Insurance"), IT("It"), MATERIALS("Materials"), MEDIA("Media"), OIL_AND_GAS("Oil & Gas"), OTHER("Other"),
	UTILITIES("Utilities"), TRANSPORTATION("Transportation"), TELECOM("Telecom"), SHIPPING_OFFSHORE("Shipping & Offshore"), 
	SERVICES("Services"), REAL_ESTATE("Real estate"), PULP_PAPER_PACKAGING("Pulp, paper, packaging"),
	PUBLIC("Public"), PROJECT("Project");

	private String sector;
	
	// GETTERS & SETTERS

	private Sector(String sector) {
		this.sector = sector;
	}

	public String getSector() {
		return sector;
	}
	
	/***
	 * Method for getting sector niceName.
	 * @return List of sectors niceName.
	 */
	public static List<String> getNiceNames() {

		List<String> sectors = new ArrayList<String>();
		for(Sector t : Sector.values()) {
			sectors.add(t.getSector());
		}
		return sectors;

	}

}
