package se.jo.danskebank.domain.constants;

import java.util.ArrayList;
import java.util.List;

/*** 
 * Constants for DbRating.
 * A rating of the credit quality of the company determined by Danske Bank. 
 * @author oscarlothman/jonathanvlajkovic
 *
 */
public enum DbRating {

	A1_1("A1-1"), A2_1("A2-1"), A2_2("A2-2"), A2_3("A2-3"), A3_1("A3-1"), A3_2("A3-2"), A3_3("A3-3"),
	A4_1("A4-1"), A4_2("A4-2"), A4_3("A4-3"), A5_1("A5-1"), A5_2("A5-2"), A5_3("A5-3"), A6_1("A6-1"),
	A6_2("A6-2"), A6_3("A6-3"), A7_1("A7-1"), A7_2("A7-2"), A7_3("A7-3"), B1_1("B1-1"), B1_2("B1-2"),
	B1_3("B1-3"), B2_1("B2-1"), B2_2("B2-2"), B2_3("B2-3"), B3_1("B3-1"), B4_1("B4-1");

	private String dbRating;
	
	// GETTERS & SETTERS

	private DbRating(String dbRating) {
		this.dbRating = dbRating;
	}

	public String getDbRating() {
		return dbRating;
	}
	
	/***
	 * Method for getting dbRating niceName.
	 * @return List of dbRatings niceName.
	 */
	public static List<String> getNiceNames() {

		List<String> dbRatings = new ArrayList<String>();
		for(DbRating d : DbRating.values()) {
			dbRatings.add(d.getDbRating());
		}
		return dbRatings;

	}

}
