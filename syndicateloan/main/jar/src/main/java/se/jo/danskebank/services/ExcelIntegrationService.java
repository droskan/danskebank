package se.jo.danskebank.services;

import java.io.FileNotFoundException;

import javax.ejb.Local;

/***
 * Interface for ExcelIntegrationService.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
@Local
public interface ExcelIntegrationService {
	
	/***
	 * Method to retrieve Borrowers from Excel sheets.
	 */
	void copyBorrowerFromExcel();
	
	/***
	 * Method to retrieve Transactions from Excel sheets.
	 */
	void copyTransactionFromExcel();
	
	/***
	 * Method to select Excel file.
	 * @param filePath
	 * @throws FileNotFoundException 
	 */
	void selectExcelFile(String filePath);

}


