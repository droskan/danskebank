package se.jo.danskebank.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.jo.danskebank.domain.Transaction;
import se.jo.danskebank.repository.BorrowerRepository;
import se.jo.danskebank.repository.TransactionRepository;

/***
 * Implementation for TransactionService.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
@Stateless
public class TransactionServiceImpl implements TransactionService {

	@Inject
	TransactionRepository transactionRepository;

	@Inject
	BorrowerRepository borrowerRepository;
	
	//CRUD

	@Override
	public Transaction getTransaction(long id) {
		return getTransactionRepository().findById(id);
	}

	@Override
	public Transaction createTransaction(Transaction transaction) {
		long id = getTransactionRepository().persist(transaction);
		return getTransactionRepository().findById(id);
	}

	@Override
	public void updateTransaction(Transaction transaction) {
		getTransactionRepository().update(transaction);
	}

	@Override
	public List<Transaction> getAllTransactions() {
		return getTransactionRepository().getAllTransactions();
	}

	@Override
	public void deleteTransaction(long id) {
		getTransactionRepository().remove(getTransaction(id));
	}

	@Override
	public List<Transaction> getTransactionsWithBorrowerId(long borrowerId) {
		return getTransactionRepository().getTransactionsWithBorrowerId(borrowerId);
	}
	
	//GETTERS & SETTERS

	public TransactionRepository getTransactionRepository() {
		return transactionRepository;
	}

	public void setTransactionRepository(TransactionRepository repository) {
		this.transactionRepository = repository;
	}

	public BorrowerRepository getBorrowerRepository() {
		return borrowerRepository;
	}

	public void setBorrowerRepository(BorrowerRepository borrowerRepository) {
		this.borrowerRepository = borrowerRepository;
	}

}
