package se.jo.danskebank.repository.jpa;

import java.util.List;

import javax.persistence.Query;

import se.jo.danskebank.domain.Price;
import se.jo.danskebank.repository.PriceRepository;

/***
 * Implementation of PriceRepository with JPA.
 * Holds the database queries for price. 
 * @author oscarlothman/jonathanvlajkovic
 *
 */
public class JpaPriceRepository extends JpaRepository<Price> implements PriceRepository{

	@SuppressWarnings("unchecked")
	@Override
	public List<Price> getAllPrices() {

		Query query = em.createQuery("select p from Price p");
		return query.getResultList();

	}

}
