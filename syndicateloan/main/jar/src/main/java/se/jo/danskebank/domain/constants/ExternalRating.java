package se.jo.danskebank.domain.constants;

import java.util.ArrayList;
import java.util.List;

/*** 
 * Constants for ExternalRating.
 * Rating determined by Standard & Poor.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
public enum ExternalRating {

	AAA("AAA"), AA_PLUS("AA+"), AA("AA"), AA_MINUS("AA-"), A_PLUS("A+"), A("A"), A_MINUS("A-"), BBB_PLUS("BBB+"), 
	BBB("BBB"), BBB_MINUS("BBB-"), BB_PLUS("BB+"), BB("BB"), BB_MINUS("BB-"),B_PLUS("B+"), B("B"), B_MINUS("B-"),
	EAAA("EAAA"), EAA_PLUS("EAA+"), EAA("EAA"), EAA_MINUS("EAA-"), EA_PLUS("EA+"), EA("EA"), EA_MINUS("EA-"), 
	EBBB_PLUS("EBBB+"), EBBB("EBBB"), EBBB_MINUS("EBBB-"), EBB_PLUS("EBB+"), EBB("EBB"), EBB_MINUS("EBB-"),
	EB_PLUS("EB+"), EB("EB"), EB_MINUS("EB-");

	private String externalRating;
	
	// GETTERS & SETTERS

	private ExternalRating(String externalRating){
		this.externalRating = externalRating;
	}

	public String getExternalRating() {
		return externalRating;
	}
	
	/***
	 * Method for getting rating niceName.
	 * @return List of ratings niceName.
	 */
	public static List<String> getNiceNames() {

		List<String> ratings = new ArrayList<String>();
		for(ExternalRating t : ExternalRating.values()) {
			ratings.add(t.getExternalRating());
		}
		return ratings;

	}

}
