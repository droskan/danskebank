package se.jo.danskebank.solr.services;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.solr.common.SolrInputDocument;
import org.joda.time.DateTime;

import se.jo.danskebank.domain.Borrower;
import se.jo.danskebank.domain.Price;
import se.jo.danskebank.domain.Transaction;
import se.jo.danskebank.services.TransactionService;

/***
 * Implementation for SolrIndexService.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
@Stateless
public class SolrIndexServiceImpl implements SolrIndexService{
	
	private Logger log = Logger.getLogger(SolrIndexServiceImpl.class.getName());

	@Inject
	private TransactionService transactionService;

	@Inject
	private SolrServerService solrServerService;


	@Override
	public void createSolrDocTransaction(Transaction transaction) {
		SolrInputDocument doc = createSolrDoc(transaction);
		getSolrServerService().commitDocToSolrServer(doc);
	}

	@Override
	public void indexAllTransactions() {

		List<Transaction> transactions = getTransactionService().getAllTransactions();
		log.fine("Number of Transaction from DB: " + transactions.size());
		List<SolrInputDocument> docs = new ArrayList<SolrInputDocument>();

		for(Transaction transaction: transactions) {
			SolrInputDocument doc = createSolrDoc(transaction);
			docs.add(doc);
		}
		
		log.fine("Number of docs commited: " + docs.size());
		getSolrServerService().commitDocumentsToSolr(docs);

	}
	
	/***
	 * ???
	 * @param transaction
	 * @return
	 */
	public SolrInputDocument createSolrDoc(Transaction transaction) {

		DateTime date= new DateTime() ;

		Borrower borrower = transaction.getBorrower();
		Price price = transaction.getPrice();

		SolrInputDocument loanDoc = new SolrInputDocument();
		loanDoc.addField("id", date.getMillisOfDay() + Math.random() * 1000);

		//Borrower
		loanDoc.addField("borrowerId", borrower.getId());
		loanDoc.addField("name", borrower.getName());
		loanDoc.addField("nationality", borrower.getNationality());
		loanDoc.addField("sector", borrower.getSector());
		loanDoc.addField("debtCategory", borrower.getDebtCategory());
		loanDoc.addField("dbRating", borrower.getDbRating());
		loanDoc.addField("externalRating", borrower.getExternalRating());

		//Transaction
		loanDoc.addField("transactionId", transaction.getId());
		loanDoc.addField("type", transaction.getType());
		loanDoc.addField("facility", transaction.getFacility());
		loanDoc.addField("status", transaction.getStatus());
		loanDoc.addField("amount", transaction.getAmount());
		loanDoc.addField("maturity", transaction.getMaturity());
		loanDoc.addField("extensionOptions", transaction.getExtensionOptions());
		loanDoc.addField("year", transaction.getYear());
		loanDoc.addField("month", transaction.getMonth());

		//Price
		loanDoc.addField("priceId", price.getId());
		loanDoc.addField("upFrontFee", price.getUpFrontFee());
		loanDoc.addField("margin", price.getMargin());
		loanDoc.addField("utilizationFee", price.getUtilizationFee());
		loanDoc.addField("fullyDrawnPrice", price.getFullyDrawnPrice());
		loanDoc.addField("comments", price.getComments());

		return loanDoc;

	}
	
	// GETTERS & SETTERS

	public TransactionService getTransactionService() {
		return transactionService;
	}

	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}

	public SolrServerService getSolrServerService() {
		return solrServerService;
	}

	public void setSolrServerService(SolrServerService solrServerService) {
		this.solrServerService = solrServerService;
	}

}
