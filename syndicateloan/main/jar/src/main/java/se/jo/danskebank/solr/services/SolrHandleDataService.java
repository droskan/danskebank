package se.jo.danskebank.solr.services;

import java.util.List;

import javax.ejb.Local;

import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import se.jo.danskebank.domain.Loan;
import se.jo.danskebank.domain.Transaction;
import se.jo.danskebank.domain.constants.DomainType;

/***
 * Interface for SolrHandleDataService.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
@Local
public interface SolrHandleDataService {
	
	/***
	 * Method for generating a Solr query for retrieving data from the indexing and decides how the results are sorted.
	 * @param queries
	 * @param sortField
	 * @return QueryResponse.
	 */
	QueryResponse generateQuery(List<String> queries, String sortField);
	
	/***
	 * Method for handling Solr response and creates Loan objects out of the data.
	 * @param response
	 * @return List of Loans.
	 */
	List<Loan> getSolrSearchResults(QueryResponse response);
	
	/***
	 * Updating existing Transaction index with id.
	 * @param transactionId
	 */
	void updateIndex(long transactionId);
	
	/***
	 * Method to retrieve a Transaction from SolrDocument.
	 * @param doc
	 * @return Transaction.
	 */
	Transaction getTransaction(SolrDocument doc);
	
	/***
	 * Method to retrieve a SolrDocument with Borrower id.
	 * @param borrowerId
	 * @param domainTypeId
	 * @return List of SolrDocument.
	 */
	SolrDocumentList getSolrDocsWithDomainId(long borrowerId, DomainType domainTypeId);

}
