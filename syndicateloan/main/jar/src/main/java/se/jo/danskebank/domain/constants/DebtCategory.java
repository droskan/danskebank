package se.jo.danskebank.domain.constants;

import java.util.ArrayList;
import java.util.List;

/*** 
 * Constants for DebtCategory. 
 * A category created by Danske Bank to classify the company's financing profile.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
public enum DebtCategory {

	ONE("1"), TWO("2"), THREE("3"), FOUR("4"), FIVE("5");

	private String debtCategory;

	// GETTERS & SETTERS
	
	private DebtCategory(String debtCategory) {
		this.debtCategory= debtCategory;
	}

	public String getDebtCategory() {
		return debtCategory;
	}
	
	/***
	 * Method for getting debtCategory niceName.
	 * @return List of debtCategorys niceName.
	 */
	public static List<String> getNiceNames() {

		List<String> debtCategorys = new ArrayList<String>();
		for(DebtCategory t : DebtCategory.values()) {
			debtCategorys.add(t.getDebtCategory());
		}
		return debtCategorys;

	}

}
