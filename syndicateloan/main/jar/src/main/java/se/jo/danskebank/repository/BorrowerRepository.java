package se.jo.danskebank.repository;

import java.util.List;

import javax.ejb.Local;

import se.jo.danskebank.domain.Borrower;

/***
 * Interface for BorrowerRepository. Handles repository calls that is explicit for for borrowers.
 * Extends BaseRepository for basic CRUD functionality.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
@Local
public interface BorrowerRepository extends BaseRepository<Borrower> {
	
	/***
	 * Method to fetch all Borrowers from the database.
	 * @return List of all Borrowers.
	 */
	List<Borrower> getAllBorrowers();
	
	/***
	 * Method to fetch Borrower by name from database.
	 * @param name of the borrower
	 * @return Borrower.
	 */
	Borrower getBorrowerByName(String name);

}
