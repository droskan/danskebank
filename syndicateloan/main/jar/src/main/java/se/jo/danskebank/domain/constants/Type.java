package se.jo.danskebank.domain.constants;

import java.util.ArrayList;
import java.util.List;

/*** 
 * Constants for Type.
 * Says if the loan is syndicated between many banks or if it is a bilateral loan.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
public enum Type {

	MULTIBANK("Multibank"), BILATERAL("Bilateral");

	private String type;
	
	// GETTERS & SETTERS

	private Type(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
	
	/***
	 * Method for getting status niceName.
	 * @return List of types niceName.
	 */
	public static List<String> getNiceNames() {

		List<String> types = new ArrayList<String>();
		for(Type t : Type.values()) {
			types.add(t.getType());
		}
		return types;

	}

}
