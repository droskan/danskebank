package se.jo.danskebank.mvc.borrower;

import java.util.logging.Logger;

import javax.inject.Inject;
import javax.validation.Valid;

import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.jo.danskebank.domain.Borrower;
import se.jo.danskebank.domain.Transaction;
import se.jo.danskebank.domain.constants.DbRating;
import se.jo.danskebank.domain.constants.DebtCategory;
import se.jo.danskebank.domain.constants.DomainType;
import se.jo.danskebank.domain.constants.ExternalRating;
import se.jo.danskebank.domain.constants.Nationality;
import se.jo.danskebank.domain.constants.Sector;
import se.jo.danskebank.services.BorrowerService;
import se.jo.danskebank.services.TransactionService;
import se.jo.danskebank.solr.services.SolrHandleDataService;
import se.jo.danskebank.solr.services.SolrServerService;

/***
 * Controller for edit borrowers.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
@Controller
@RequestMapping("/editBorrower/{borrowerId}")
public class EditBorrowerController {
	
	Logger log = Logger.getLogger(EditBorrowerController.class.getName());

	@Inject
	private BorrowerService borrowerService;
	
	@Inject
	private TransactionService transactionService;
	
	@Inject
	private SolrHandleDataService solrHandleDataService;
	
	@Inject
	private SolrServerService serverService;
	
	// Shows the edit borrower page.
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView index(@PathVariable long borrowerId) {
		
		EditBorrowerBean bean = new EditBorrowerBean();
		Borrower borrower = getBorrowerService().getBorrower(borrowerId);
		bean.copyBorrowerValuesToBean(borrower);
		
		ModelAndView mav = new ModelAndView("borrower/editBorrower");
		mav.addObject("editBorrowerBean", bean);
		mav.addObject("nationalities", Nationality.values());
		mav.addObject("sectors", Sector.values());
		mav.addObject("dbRatings", DbRating.values());
		mav.addObject("externalRatings", ExternalRating.values());
		mav.addObject("debtCategories", DebtCategory.values());
		return mav;

	}
	
	// Handel the submit from edit borrower page.
	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView handleSubmit(@Valid EditBorrowerBean bean) throws Exception {
		log.fine("in post delete");
		if(bean.getName() == null) {
			log.fine("name == null");
			long id = bean.getBorrowerId();
			
			for(Transaction t:getTransactionService().getTransactionsWithBorrowerId(id)) {
				getTransactionService().deleteTransaction(t.getId());
			}
			SolrDocumentList docs = getSolrHandleDataService().getSolrDocsWithDomainId(id, DomainType.BORROWER_ID);
			
			for(SolrDocument doc : docs) {
				getServerService().deleteById(doc.getFieldValue("id")+"");
			}
			getBorrowerService().deleteBorrower(id);
		}else {
		
		Borrower borrower = getBorrowerService().getBorrower(bean.getBorrowerId());
		bean.copyBeanValuesToBorrower(borrower);
		getBorrowerService().updateBorrower(borrower); }
		
		return new ModelAndView("redirect:/createBorrower.html");
		
	}
	
	// GETTERS & SETTERS

	public BorrowerService getBorrowerService() {
		return borrowerService;
	}

	public void setBorrowerService(BorrowerService borrowerService) {
		this.borrowerService = borrowerService;
	}

	public TransactionService getTransactionService() {
		return transactionService;
	}

	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}

	public SolrHandleDataService getSolrHandleDataService() {
		return solrHandleDataService;
	}

	public void setSolrHandleDataService(SolrHandleDataService solrService) {
		this.solrHandleDataService = solrService;
	}

	public SolrServerService getServerService() {
		return serverService;
	}

	public void setServerService(SolrServerService serverService) {
		this.serverService = serverService;
	}

}
