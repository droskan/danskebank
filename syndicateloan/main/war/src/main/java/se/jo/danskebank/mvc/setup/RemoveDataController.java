package se.jo.danskebank.mvc.setup;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import se.jo.danskebank.domain.Borrower;
import se.jo.danskebank.domain.Transaction;
import se.jo.danskebank.services.BorrowerService;
import se.jo.danskebank.services.PriceService;
import se.jo.danskebank.services.TransactionService;
import se.jo.danskebank.solr.services.SolrServerService;

/***
 * Controller for remove all data in the database and the Solr index data.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
@Controller
public class RemoveDataController {
	
	@Inject
	private BorrowerService borrowerService;
	
	@Inject
	private TransactionService transactionService;
	
	@Inject
	private PriceService priceService;
	
	@Inject
	private SolrServerService solrServerService;
	
	@RequestMapping("/removeData.html")
	public ModelAndView removeData() {
		List<Transaction> transactions = getTransactionService().getAllTransactions();
		for (Transaction transaction : transactions) {
			getTransactionService().deleteTransaction(transaction.getId());
		}
		List<Borrower> borrowers = getBorrowerService().getAllBorrowers();
		for (Borrower borrower : borrowers) {
			getBorrowerService().deleteBorrower(borrower.getId());
		}
		getSolrServerService().deleteAllSolrData();
		return new ModelAndView("redirect:/setup.html");
	}
	
	// GETTERS & SETTERS
	
	public BorrowerService getBorrowerService() {
		return borrowerService;
	}

	public void setBorrowerService(BorrowerService borrowerService) {
		this.borrowerService = borrowerService;
	}

	public TransactionService getTransactionService() {
		return transactionService;
	}

	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}

	public PriceService getPriceService() {
		return priceService;
	}

	public void setPriceService(PriceService priceService) {
		this.priceService = priceService;
	}

	public SolrServerService getSolrServerService() {
		return solrServerService;
	}

	public void setSolrServerService(SolrServerService solrServerService) {
		this.solrServerService = solrServerService;
	}

}
