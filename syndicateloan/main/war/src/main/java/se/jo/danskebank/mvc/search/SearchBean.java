package se.jo.danskebank.mvc.search;

/***
 * Bean that holds the search parameters.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
public class SearchBean {

	//Borrower
	private String name;
	private String nationality;
	private String sector;
	private String debtCategory;
	private String dbRating;
	private String externalRating;

	//Transaction
	private String month;
	private String year;
	private String amount;
	private String maturity;
	private String extensionOptions;
	private String type;
	private String facility;
	private String status;

	//Price
	private String upFrontFee;
	private String margin;
	private String utilizationFee;
	private String fullyDrawnPrice;
	
	private String sortField;
	
	// GETTERS & SETTERS

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public String getDebtCategory() {
		return debtCategory;
	}

	public void setDebtCategory(String debtCategory) {
		this.debtCategory = debtCategory;
	}

	public String getDbRating() {
		return dbRating;
	}

	public void setDbRating(String dbRating) {
		this.dbRating = dbRating;
	}

	public String getExternalRating() {
		return externalRating;
	}

	public void setExternalRating(String externalRating) {
		this.externalRating = externalRating;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getMaturity() {
		return maturity;
	}

	public void setMaturity(String maturity) {
		this.maturity = maturity;
	}

	public String getExtensionOptions() {
		return extensionOptions;
	}

	public void setExtensionOptions(String extensionOptions) {
		this.extensionOptions = extensionOptions;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFacility() {
		return facility;
	}

	public void setFacility(String facility) {
		this.facility = facility;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUpFrontFee() {
		return upFrontFee;
	}

	public void setUpFrontFee(String upFrontFee) {
		this.upFrontFee = upFrontFee;
	}

	public String getMargin() {
		return margin;
	}

	public void setMargin(String margin) {
		this.margin = margin;
	}

	public String getUtilizationFee() {
		return utilizationFee;
	}

	public void setUtilizationFee(String utilizationFee) {
		this.utilizationFee = utilizationFee;
	}

	public String getFullyDrawnPrice() {
		return fullyDrawnPrice;
	}

	public void setFullyDrawnPrice(String fullyDrawnPrice) {
		this.fullyDrawnPrice = fullyDrawnPrice;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

}
