package se.jo.danskebank.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/***
 * Controller for the application main page.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
@Controller
public class IndexController {
	
	// Shows the applications main page.
	@RequestMapping("/index.html")
	public ModelAndView index() {
		return new ModelAndView();
	}

}
