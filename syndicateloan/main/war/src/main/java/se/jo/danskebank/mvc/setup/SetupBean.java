package se.jo.danskebank.mvc.setup;

/***
 * Bean that holds the Excel file path.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
public class SetupBean {
	
	private String excelFilePath;

	// GETTERS & SETTERS
	
	public String getExcelFilePath() {
		return excelFilePath;
	}

	public void setExcelFilePath(String excelFilePath) {
		this.excelFilePath = excelFilePath;
	}

}
