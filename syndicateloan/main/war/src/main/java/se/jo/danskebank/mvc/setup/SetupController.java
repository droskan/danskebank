package se.jo.danskebank.mvc.setup;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.jo.danskebank.services.ExcelIntegrationService;
import se.jo.danskebank.solr.services.SolrIndexService;

/***
 * Controller for setting up the application data.
 * Imports the data from the selected Excel file and index all the data with Solr.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
@Controller
@RequestMapping("/setup.html")
public class SetupController {
	
	@Inject
	private ExcelIntegrationService excelIntegrationService;
	
	@Inject
	private SolrIndexService solrTransactionService;
	
	// Shows the setup page.
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView index() {
		
		SetupBean bean = new SetupBean();
		
		ModelAndView mav = new ModelAndView("setup/setup");
		mav.addObject("setupBean", bean);

		return mav;

	}
	
	// Handel the submit from setup page.
	@RequestMapping(method = RequestMethod.POST) 
	public ModelAndView handleSubmit(@Valid SetupBean bean, BindingResult errors) {
		
		if (errors.hasErrors()) {
			ModelAndView mav = new ModelAndView("setup/setup");
			mav.addObject("setupBean", bean);
			mav.addObject("excelFilePath", bean.getExcelFilePath());
			return mav;
		}
		
		getExcelIntegrationService().selectExcelFile(bean.getExcelFilePath());
		getExcelIntegrationService().copyBorrowerFromExcel();
		getSolrTransactionService().indexAllTransactions();
		return new ModelAndView("redirect:/setup.html");
	}
	
	// GETTERS & SETTERS

	public ExcelIntegrationService getExcelIntegrationService() {
		return excelIntegrationService;
	}

	public void setExcelIntegrationService(
			ExcelIntegrationService excelIntegrationService) {
		this.excelIntegrationService = excelIntegrationService;
	}

	public SolrIndexService getSolrTransactionService() {
		return solrTransactionService;
	}

	public void setSolrTransactionService(SolrIndexService solrTransactionService) {
		this.solrTransactionService = solrTransactionService;
	}

}
