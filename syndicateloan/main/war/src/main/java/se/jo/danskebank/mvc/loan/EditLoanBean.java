package se.jo.danskebank.mvc.loan;

import se.jo.danskebank.domain.Borrower;
import se.jo.danskebank.domain.Price;
import se.jo.danskebank.domain.Transaction;

/***
 * Bean that holds loan data.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
public class EditLoanBean {
	
	private long transactionId;
	private String type;
	private String facility;
	private String status;
	private long amount;
	private int maturity;
	private int extensionOptions;
	private long borrowerId;
	private double upFrontFee;
	private double margin;
	private double utilizationFee;
	private double fullyDrawnPrice;
	private int month;
	private int year;
	private String comment;
	
	/***
	 * Puts data from the Loan to the form in edit Loan page.
	 * @param transaction
	 */
	public void copyLoanValuesToBean(Transaction transaction) {
		setTransactionId(transaction.getId());
		setType(transaction.getType());
		setFacility(transaction.getFacility());
		setStatus(transaction.getStatus());
		setAmount(transaction.getAmount());
		setMaturity(transaction.getMaturity());
		setExtensionOptions(transaction.getExtensionOptions());
		setBorrowerId(transaction.getBorrower().getId());
		setUpFrontFee(transaction.getPrice().getUpFrontFee());
		setMargin(transaction.getPrice().getMargin());
		setUtilizationFee(transaction.getPrice().getUtilizationFee());
		setFullyDrawnPrice(transaction.getPrice().getFullyDrawnPrice());
		setMonth(transaction.getMonth());
		setYear(transaction.getYear());
		setComment(transaction.getPrice().getComments());
	}
	
	/***
	 * Puts data from the edit Loan form to the Loan object.
	 * @param transaction
	 * @param borrower
	 */
	public void copyBeanValuesToLoan(Transaction transaction, Borrower borrower) {
		transaction.setId(getTransactionId());
		transaction.setType(getType());
		transaction.setFacility(getFacility());
		transaction.setStatus(getStatus());
		transaction.setAmount(getAmount());
		transaction.setMaturity(getMaturity());
		transaction.setExtensionOptions(getExtensionOptions());
		transaction.setBorrower(borrower);
		transaction.setMonth(getMonth());
		transaction.setYear(getYear());

		Price price = transaction.getPrice();
		if(price == null) {
			price = new Price();
		}
		price.setUpFrontFee(getUpFrontFee());
		price.setMargin(getMargin());
		price.setUtilizationFee(getUtilizationFee());
		price.setFullyDrawnPrice(getFullyDrawnPrice());
		price.setComments(getComment());
		transaction.setPrice(price);
	}
	
	// GETTERS & SETTERS

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFacility() {
		return facility;
	}

	public void setFacility(String facility) {
		this.facility = facility;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public int getMaturity() {
		return maturity;
	}

	public void setMaturity(int maturity) {
		this.maturity = maturity;
	}

	public int getExtensionOptions() {
		return extensionOptions;
	}

	public double getUpFrontFee() {
		return upFrontFee;
	}

	public void setUpFrontFee(double upFrontFee) {
		this.upFrontFee = upFrontFee;
	}

	public double getMargin() {
		return margin;
	}

	public void setMargin(double margin) {
		this.margin = margin;
	}

	public double getUtilizationFee() {
		return utilizationFee;
	}

	public void setUtilizationFee(double utilizationFee) {
		this.utilizationFee = utilizationFee;
	}

	public double getFullyDrawnPrice() {
		return fullyDrawnPrice;
	}

	public void setFullyDrawnPrice(double fullyDrawnPrice) {
		this.fullyDrawnPrice = fullyDrawnPrice;
	}

	public void setExtensionOptions(int extensionOptions) {
		this.extensionOptions = extensionOptions;
	}
	public long getBorrowerId() {
		return borrowerId;
	}
	public void setBorrowerId(long borrowerId) {
		this.borrowerId = borrowerId;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}
