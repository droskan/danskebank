package se.jo.danskebank.mvc.search;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.validation.Valid;

import org.apache.solr.client.solrj.response.QueryResponse;
import org.joda.time.DateTime;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.jo.danskebank.domain.Loan;
import se.jo.danskebank.domain.constants.DbRating;
import se.jo.danskebank.domain.constants.DebtCategory;
import se.jo.danskebank.domain.constants.ExternalRating;
import se.jo.danskebank.domain.constants.Facility;
import se.jo.danskebank.domain.constants.Month;
import se.jo.danskebank.domain.constants.Nationality;
import se.jo.danskebank.domain.constants.Sector;
import se.jo.danskebank.domain.constants.SortFields;
import se.jo.danskebank.domain.constants.Status;
import se.jo.danskebank.domain.constants.Type;
import se.jo.danskebank.services.BorrowerService;
import se.jo.danskebank.services.ExcelIntegrationService;
import se.jo.danskebank.solr.services.SolrHandleDataService;
import se.jo.danskebank.solr.services.SolrIndexService;
import se.jo.danskebank.solr.services.SolrServerService;

/***
 * Controller for search the data. 
 * Takes the selected search parameters and shows the result.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
@Controller
@RequestMapping("/search.html")
public class SearchController {
	
	Logger log = Logger.getLogger(SearchController.class.getName());

	@Inject
	ExcelIntegrationService excelIntegrationService;

	@Inject
	BorrowerService borrowerService;

	@Inject
	SolrServerService solrServerService;

	@Inject
	SolrIndexService solrTransactionService;

	@Inject
	SolrHandleDataService dataService;

	private List<Loan> loans = new ArrayList<Loan>();
	private List<SearchParameters> parameters = new ArrayList<SearchController.SearchParameters>();
	
	// Shows the search page.
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView index() {

		SearchBean bean = new SearchBean();

		ModelAndView mav = new ModelAndView("search/search");
		mav.addObject("dbRatings", DbRating.values());
		mav.addObject("externalRatings", ExternalRating.values());
		mav.addObject("types", Type.values());
		mav.addObject("facilities", Facility.values());
		mav.addObject("statuses", Status.values());
		mav.addObject("debtCategories", DebtCategory.values());
		mav.addObject("nationalities", Nationality.values());
		mav.addObject("sectors", Sector.values());
		mav.addObject("months", Month.getNiceNames());
		mav.addObject("sortFields", SortFields.getNiceNames());
		mav.addObject("years", years());
		mav.addObject("searchBean", bean);
		mav.addObject("loans", getLoans());
		if(getParameters().size() != 0) {
			log.fine("Parameter: " + getParameters().get(0).getParameter());
		}
		mav.addObject("params", getParameters());

		return mav;

	}
	
	// Handel the submit from search page.
	@RequestMapping(method = RequestMethod.POST) 
	public ModelAndView handleSubmit(@Valid SearchBean bean) {

		if(loans.size() != 0) {
			loans.clear();
		}
		List<String> queries = new ArrayList<String>();
		queries.add(replaceCharsForRangeSearch(bean.getAmount(), "amount"));
		queries.add(checkIfTheresAnValue(bean.getDbRating(), "dbRating"));
		queries.add(checkIfTheresAnValue(bean.getDebtCategory(), "debtCategory"));
		queries.add(replaceCharsForRangeSearch(bean.getExtensionOptions(), "extensionOptions"));
		queries.add(checkIfTheresAnValue(bean.getExternalRating(), "externalRating"));
		queries.add(checkIfTheresAnValue(bean.getFacility(), "facility"));
		queries.add(replaceCharsForRangeSearch(bean.getFullyDrawnPrice(), "fullyDrawnPrice"));
		queries.add(replaceCharsForRangeSearch(bean.getMargin(), "margin"));
		queries.add(replaceCharsForRangeSearch(bean.getMaturity(), "maturity"));
		queries.add(checkIfTheresAnValue(bean.getMonth(), "month"));
		if(bean.getName() != "") {
			queries.add(bean.getName() + " name"); 
		}
		queries.add(checkIfTheresAnValue(bean.getNationality(), "nationality"));
		queries.add(checkIfTheresAnValue(bean.getSector(), "sector"));
		queries.add(checkIfTheresAnValue(bean.getStatus(), "status"));
		queries.add(checkIfTheresAnValue(bean.getType(), "type"));
		queries.add(replaceCharsForRangeSearch(bean.getUpFrontFee(), "upFrontFee"));
		queries.add(replaceCharsForRangeSearch(bean.getUtilizationFee(), "utilizationFee"));
		queries.add(checkIfTheresAnValue(bean.getYear(), "year"));
		

		QueryResponse response = getDataService().generateQuery(queries, bean.getSortField());

		loans = getDataService().getSolrSearchResults(response);
		
		return new ModelAndView("redirect:/search.html");

	}

	// GETTERS & SETTERS

	public ExcelIntegrationService getExcelIntegrationService() {
		return excelIntegrationService;
	}

	public void setExcelIntegrationService(
			ExcelIntegrationService excelIntegrationService) {
		this.excelIntegrationService = excelIntegrationService;
	}

	public SolrHandleDataService getDataService() {
		return dataService;
	}

	public void setDataService(SolrHandleDataService dataService) {
		this.dataService = dataService;
	}

	public BorrowerService getBorrowerService() {
		return borrowerService;
	}

	public void setBorrowerService(BorrowerService borrowerService) {
		this.borrowerService = borrowerService;
	}

	public SolrServerService getSolrServerService() {
		return solrServerService;
	}

	public void setSolrServerService(SolrServerService solrServerService) {
		this.solrServerService = solrServerService;
	}

	public SolrIndexService getSolrTransactionService() {
		return solrTransactionService;
	}

	public void setSolrTransactionService(SolrIndexService solrTransactionService) {
		this.solrTransactionService = solrTransactionService;
	}

	public List<Loan> getLoans() {
		return loans;
	}

	public void setLoans(List<Loan> loans) {
		this.loans = loans;
	}

	private String replaceCharsForRangeSearch(String query, String nameOfField) {
		if(query != "") {
			query = nameOfField + ":[" + query.replace(",", " TO ") + "]";
			SearchParameters sp = new SearchParameters(query);
			getParameters().add(sp);
		}
		return query;
	}

	private String checkIfTheresAnValue(String query, String nameOfField) {
		if(query != "") {
			query = nameOfField + ": " + query;
			SearchParameters sp = new SearchParameters(query);
			getParameters().add(sp);
		}
		return query;
	}

	public List<Integer> years() {
		List<Integer> years = new ArrayList<Integer>();
		DateTime dt = new DateTime();

		for(int i = 2012; i<= dt.getYear(); i++ ) {
			years.add(i);
		}
		return years;
	}
	
	public List<SearchParameters> getParameters() {
		return parameters;
	}

	public void setParameters(List<SearchParameters> parameters) {
		this.parameters = parameters;
	}

	public class SearchParameters {
		
		private String parameter;
		
		public SearchParameters(String parameter) {
			this.parameter = parameter;
		}

		public String getParameter() {
			return parameter;
		}

		public void setParameter(String parameter) {
			this.parameter = parameter;
		}
	}
	
	

}
