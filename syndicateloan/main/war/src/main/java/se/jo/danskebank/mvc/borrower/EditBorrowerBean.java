package se.jo.danskebank.mvc.borrower;

import se.jo.danskebank.domain.Borrower;

/***
 * Bean that holds borrower data.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
public class EditBorrowerBean {

	private long borrowerId;
	private String name;
	private String nationality;
	private String sector;
	private int debtCategory;
	private String dbRating;
	private String externalRating;
	
	/***
	 * Puts data from the Borrower to the form in edit Borrower page.
	 * @param borrower
	 */
	public void copyBorrowerValuesToBean(Borrower borrower) {
		setBorrowerId(borrower.getId());
		setName(borrower.getName());
		setNationality(borrower.getNationality());
		setSector(borrower.getSector());
		setDebtCategory(borrower.getDebtCategory());
		setDbRating(borrower.getDbRating());
		setExternalRating(borrower.getExternalRating());
	}
	
	/***
	 * Puts data from the edit Borrower form to the Borrower object.
	 * @param borrower
	 */
	public void copyBeanValuesToBorrower(Borrower borrower) {
		borrower.setId(getBorrowerId());
		borrower.setName(getName());
		borrower.setNationality(getNationality());
		borrower.setSector(getSector());
		borrower.setDebtCategory(getDebtCategory());
		borrower.setDbRating(getDbRating());
		borrower.setExternalRating(getExternalRating());
	}
	
	// GETTERS & SETTERS

	public long getBorrowerId() {
		return borrowerId;
	}

	public void setBorrowerId(long borrowerId) {
		this.borrowerId = borrowerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public int getDebtCategory() {
		return debtCategory;
	}

	public void setDebtCategory(int debtCategory) {
		this.debtCategory = debtCategory;
	}

	public String getDbRating() {
		return dbRating;
	}

	public void setDbRating(String dbRating) {
		this.dbRating = dbRating;
	}

	public String getExternalRating() {
		return externalRating;
	}

	public void setExternalRating(String externalRating) {
		this.externalRating = externalRating;
	}

}
