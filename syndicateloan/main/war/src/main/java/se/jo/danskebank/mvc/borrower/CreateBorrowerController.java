package se.jo.danskebank.mvc.borrower;

import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.jo.danskebank.domain.Borrower;
import se.jo.danskebank.domain.constants.DbRating;
import se.jo.danskebank.domain.constants.DebtCategory;
import se.jo.danskebank.domain.constants.ExternalRating;
import se.jo.danskebank.domain.constants.Nationality;
import se.jo.danskebank.domain.constants.Sector;
import se.jo.danskebank.services.BorrowerService;

/***
 * Controller for creating borrowers.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
@Controller
@RequestMapping("/createBorrower.html")
public class CreateBorrowerController {

	@Inject
	private BorrowerService borrowerService;
	
	// Shows the create borrower page.
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView createBorrower() {

		EditBorrowerBean bean = new EditBorrowerBean();
		ModelAndView mav = new ModelAndView("borrower/createBorrower");

		List<Borrower> borrowers = getBorrowerService().getAllBorrowers();
		mav.addObject("dbRatings", DbRating.getNiceNames());
		mav.addObject("ratings", ExternalRating.getNiceNames());
		mav.addObject("debtCategories", DebtCategory.getNiceNames());
		mav.addObject("nationalities", Nationality.values());
		mav.addObject("sectors", Sector.getNiceNames());
		mav.addObject("editBorrowerBean", bean);
		mav.addObject("borrowers", borrowers);

		return mav;

	}
	
	// Handel the submit from create borrower page.
	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView handleSubmit(@Valid EditBorrowerBean bean) throws Exception {

		Borrower borrower = new Borrower();
		bean.copyBeanValuesToBorrower(borrower);
		getBorrowerService().createBorrower(borrower);

		return new ModelAndView("redirect:/search.html");

	}
	
	// GETTERS & SETTERS

	public BorrowerService getBorrowerService() {
		return borrowerService;
	}

	public void setBorrowerService(BorrowerService borrowerService) {
		this.borrowerService = borrowerService;
	}

}
