package se.jo.danskebank.mvc;

import java.io.FileNotFoundException;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import se.jo.danskebank.services.ExcelIntegrationService;
import se.jo.danskebank.solr.services.SolrIndexService;

/***
 * Controller that handles the Excel import and converts it to Solr-data.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
@Controller
public class ImportController {

	@Inject
	private ExcelIntegrationService excelIntegrationService;

	@Inject
	private SolrIndexService solrTransactionService;
	
	// Import data from selected Excel sheet.
	@RequestMapping("/import.html")
	public ModelAndView importexcel() {
		try {
			excelIntegrationService.selectExcelFile("/home/anderjo4/danskebank/test1.xls");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		excelIntegrationService.copyBorrowerFromExcel();
		return new ModelAndView("redirect:/search.html");

	}
	
	// Create Solr document from imported Excel data.
	@RequestMapping("/createDoc.html")
	public ModelAndView createSolrDoc() {
		solrTransactionService.indexAllTransactions();
		return new ModelAndView("redirect:/search.html");
	}

}


