package se.jo.danskebank.mvc.loan;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.validation.Valid;

import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.joda.time.DateTime;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.jo.danskebank.domain.Borrower;
import se.jo.danskebank.domain.Transaction;
import se.jo.danskebank.domain.constants.DomainType;
import se.jo.danskebank.domain.constants.Facility;
import se.jo.danskebank.domain.constants.Month;
import se.jo.danskebank.domain.constants.Status;
import se.jo.danskebank.domain.constants.Type;
import se.jo.danskebank.services.BorrowerService;
import se.jo.danskebank.services.PriceService;
import se.jo.danskebank.services.TransactionService;
import se.jo.danskebank.solr.services.SolrHandleDataService;
import se.jo.danskebank.solr.services.SolrIndexService;
import se.jo.danskebank.solr.services.SolrServerService;

/***
 * Controller for edit/create loans.
 * @author oscarlothman/jonathanvlajkovic
 *
 */
@Controller
@RequestMapping("/editLoan/{transactionId}.html")
public class EditLoanController {
	
	Logger log = Logger.getLogger(EditLoanController.class.getName());

	@Inject
	private TransactionService transactionService;

	@Inject 
	private BorrowerService borrowerService;

	@Inject
	private PriceService priceService;

	@Inject
	private SolrHandleDataService solrHandleDataService;

	@Inject
	private SolrIndexService solrIndexService;
	
	@Inject
	private SolrServerService serverService;
	
	// Shows the edit Loan page.
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView index(@PathVariable long transactionId) {

		EditLoanBean bean = new EditLoanBean();
		boolean isNewTransaction = transactionId <= 0;

		Transaction transaction = null;
		if (isNewTransaction) {
			transaction = new Transaction();
		} else {
			transaction = getTransactionService().getTransaction(transactionId);
			bean.copyLoanValuesToBean(transaction);
		}

		ModelAndView mav = new ModelAndView("loan/editLoan");
		mav.addObject("editLoanBean", bean);
		mav.addObject("types", Type.values());
		mav.addObject("facilities", Facility.values());
		mav.addObject("statuses", Status.values());

		ArrayList<Borrower> borrowers = (ArrayList<Borrower>) getBorrowerService().getAllBorrowers();
		Collections.sort(borrowers, new Comparator<Borrower>() {

			@Override
			public int compare(Borrower b1, Borrower b2) {
				
				return b1.getName().compareTo(b2.getName());
			}
		});
		
		mav.addObject("borrowers", borrowers);
		mav.addObject("months", Month.getNiceNames());
		mav.addObject("years", years());

		return mav;

	}
	
	// Handel the submit from edit Loan page.
	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView handleSubmit(@Valid EditLoanBean bean) throws Exception {
		
		if(bean.getStatus() == null) {
			long id = bean.getTransactionId();
			log.fine("id to be deleted" + id);
			getTransactionService().deleteTransaction(id);
			SolrDocumentList docs = getSolrHandleDataService().getSolrDocsWithDomainId(id, DomainType.TRANSACTION_ID);
			
			for(SolrDocument doc : docs) {
				log.fine("docs delete: " + doc);
				getServerService().deleteById(doc.getFieldValue("id")+"");
			}
			return new ModelAndView("redirect:/search.html");
		}

		Transaction transaction = null;
		if (bean.getTransactionId() > 0) {
			transaction = getTransactionService().getTransaction(bean.getTransactionId());
		} else {
			transaction = new Transaction();
		}

		Borrower borrower = getBorrowerService().getBorrower(bean.getBorrowerId());
		bean.copyBeanValuesToLoan(transaction, borrower);

		if (transaction.getId() > 0) {
			getTransactionService().updateTransaction(transaction);
			getSolrHandleDataService().updateIndex(transaction.getId());
		} else {
			getTransactionService().createTransaction(transaction);
			getSolrIndexService().createSolrDocTransaction(transaction);
		}

		return new ModelAndView("redirect:/search.html");

	}
	
	/***
	 * Creates list of years.
	 * @return List of years.
	 */
	public List<Integer> years() {
		List<Integer> years = new ArrayList<Integer>();
		DateTime dt = new DateTime();

		for(int i = 2012; i<= dt.getYear(); i++ ) {
			years.add(i);
		}
		return years;
	}
	
	// GETTERS & SETTERS

	public TransactionService getTransactionService() {
		return transactionService;
	}

	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}

	public BorrowerService getBorrowerService() {
		return borrowerService;
	}

	public void setBorrowerService(BorrowerService borrowerService) {
		this.borrowerService = borrowerService;
	}

	public PriceService getPriceService() {
		return priceService;
	}

	public void setPriceService(PriceService priceService) {
		this.priceService = priceService;
	}

	public SolrHandleDataService getSolrHandleDataService() {
		return solrHandleDataService;
	}

	public void setSolrHandleDataService(SolrHandleDataService solrHandleDataService) {
		this.solrHandleDataService = solrHandleDataService;
	}

	public SolrIndexService getSolrIndexService() {
		return solrIndexService;
	}

	public void setSolrIndexService(SolrIndexService solrIndexService) {
		this.solrIndexService = solrIndexService;
	}

	public SolrServerService getServerService() {
		return serverService;
	}

	public void setServerService(SolrServerService serverService) {
		this.serverService = serverService;
	}

}
