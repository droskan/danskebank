<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
<title>Danskebank - Syndicateloan</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="<%=request.getContextPath()%>/style/bootstrap.css"
	type="text/css" rel="stylesheet" />
<link href="<%=request.getContextPath()%>/style/bootstrap-select.css"
	type="text/css" rel="stylesheet" />
<link href="<%=request.getContextPath()%>/style/index.css"
	type="text/css" rel="stylesheet" />
<link href="<%=request.getContextPath()%>/style/common.css"
	type="text/css" rel="stylesheet" />
<style type="text/css"></style>

</head>
<body>

	<div class="container">
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<%=request.getContextPath()%>/index.html">Syndicateloan</a>
			</div>
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="<%=request.getContextPath()%>/index.html">Home</a></li>
					<li><a href="<%=request.getContextPath()%>/search.html">Search</a></li>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown">Create<b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="<%=request.getContextPath()%>/createBorrower.html">Create borrower</a></li>
							<li class="divider"></li>
							<li><a href="<%=request.getContextPath()%>/editLoan/0.html">Create loan</a></li>
						</ul></li>
						<li><a href="<%=request.getContextPath()%>/setup.html">Setup</a></li>
				</ul>
			</div>
		</nav>
	</div>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="<%=request.getContextPath()%>/js/jquery.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="<%=request.getContextPath()%>/js/bootstrap.js"></script>
	<script src="<%=request.getContextPath()%>/js/bootstrap-select.js"></script>
	<script type="text/javascript">
		$(document).ready(function(e) {
			$('.selectpicker').selectpicker({
				style : 'btn-primary',
				size : 4
			});
		});
	</script>
</body>
</html>