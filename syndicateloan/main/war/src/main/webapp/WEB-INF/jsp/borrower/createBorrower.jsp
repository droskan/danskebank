<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
<title>Danskebank - Syndicateloan</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="<%=request.getContextPath()%>/style/bootstrap.css"
	type="text/css" rel="stylesheet" />
<link href="<%=request.getContextPath()%>/style/bootstrap-select.css"
	type="text/css" rel="stylesheet" />
<link href="<%=request.getContextPath()%>/style/index.css"
	type="text/css" rel="stylesheet" />
<link href="<%=request.getContextPath()%>/style/validation.css"
	type="text/css" rel="stylesheet" />
<style type="text/css"></style>

</head>
<body>

	<div class="container">
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand"
					href="<%=request.getContextPath()%>/index.html">Syndicateloan</a>
			</div>
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="<%=request.getContextPath()%>/index.html">Home</a></li>
					<li><a href="<%=request.getContextPath()%>/search.html">Search</a></li>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown">Create<b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a
								href="<%=request.getContextPath()%>/createBorrower.html">Create
									borrower</a></li>
							<li class="divider"></li>
							<li><a href="<%=request.getContextPath()%>/editLoan/0.html">Create
									loan</a></li>
						</ul></li>
						<li><a href="<%=request.getContextPath()%>/setup.html">Setup</a></li>
				</ul>
			</div>
		</nav>
		<form:form id="createBorrowerForm" commandName="editBorrowerBean">
			<div class="row">
				<h3>Create borrower</h3>
				<hr>
				<div class="panel-body">
					<div class="row">
						<div class="form-group">
							<label class='control-label'>Name</label>
							<form:input id="name" path="name" type="text" class="form-control" placeholder="Before creating a new borrower: use the search field below to make sure it dosen't already exists"/>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<form:select class="selectpicker" title="Nationality"
								data-width="150px" path="nationality" data-msg-required="Select nationality!" data-rule-required="true">
								<option data-hidden="true"></option>
								<form:options items="${nationalities}" />
							</form:select>
							<form:select class="selectpicker" title="Sector"
								data-width="150px" path="sector" data-msg-required="Select sector!" data-rule-required="true">
								<option data-hidden="true"></option>
								<form:options items="${sectors}" />
							</form:select>
							<form:select class="selectpicker" title="Debt category"
								data-width="150px" path="debtCategory" data-msg-required="Select debt category!" data-rule-required="true">
								<option data-hidden="true"></option>
								<form:options items="${debtCategories}" />
							</form:select>
							<form:select class="selectpicker" title="DB rating"
								data-width="150px" path="dbRating" data-msg-required="Select db rating!" data-rule-required="true">
								<option data-hidden="true"></option>
								<form:options items="${dbRatings}" />
							</form:select>
							<form:select class="selectpicker" title="External rating"
								data-width="150px" path="externalRating" data-msg-required="Select db external!" data-rule-required="true">
								<option data-hidden="true"></option>
								<form:options items="${ratings}" />
							</form:select>
						</div>
					</div>
				</div>
				<button type="submit" class="btn btn-primary">Create</button>
				<a href="<%=request.getContextPath()%>/index.html"
								class="btn btn-primary">Cancel</a>
			</div>
		</form:form>
		<div class="row">
			<h3>Edit borrower</h3>
			<hr>
			<form action="#" method="get">
				<div class="input-group">
					<!-- USE TWITTER TYPEAHEAD JSON WITH API TO SEARCH -->
					<input class="form-control" id="system-search" name="q"
						placeholder="Search for borrower" required> <span
						class="input-group-btn">
					</span>
				</div>
			</form>
			<table class="table table-list-search">
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Debt category</th>
						<th>Nationality</th>
						<th>DB rating</th>
						<th>ext (exp) rating</th>
						<th>Sector</th>
						<th class="text-center">Action</th>
					</tr>
				</thead>
				<c:forEach items="${borrowers}" var="borrower">
					<tr>
						<td>${borrower.id}</td>
						<td>${borrower.name}</td>
						<td>${borrower.debtCategory}</td>
						<td>${borrower.nationality}</td>
						<td>${borrower.dbRating}</td>
						<td>${borrower.externalRating}</td>
						<td>${borrower.sector}</td>
						<td class="text-center"><a class='btn btn-primary btn-xs'
							href="<%= request.getContextPath() %>/editBorrower/${borrower.id}.html"><span class="glyphicon glyphicon-edit"></span> Edit</a></td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>

	<!--  <p>
		<a class="btn btn btn-primary"
			href="<%=request.getContextPath()%>/import.html">Import</a>
	</p>
	<p>
		<a class="btn btn btn-primary"
			href="<%=request.getContextPath()%>/search.html">Search</a>
	</p> -->

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="<%=request.getContextPath()%>/js/jquery.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="<%=request.getContextPath()%>/js/bootstrap.js"></script>
	<script src="<%=request.getContextPath()%>/js/bootstrap-select.js"></script>
	<!-- Validation -->
	<script src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
	
	<script type="text/javascript">
		$(document).ready(function(e) {
			$('.selectpicker').selectpicker({
				style : 'btn-primary',
				size : 4
			});
		});
	</script>
	<script type="text/javascript">
	$(document).ready(function() {
    var activeSystemClass = $('.list-group-item.active');

    //something is entered in search form
    $('#system-search').keyup( function() {
       var that = this;
        // affect all table rows on in systems table
        var tableBody = $('.table-list-search tbody');
        var tableRowsClass = $('.table-list-search tbody tr');
        $('.search-sf').remove();
        tableRowsClass.each( function(i, val) {
        
            //Lower text for case insensitive
            var rowText = $(val).text().toLowerCase();
            var inputText = $(that).val().toLowerCase();
            if(inputText != '')
            {
                $('.search-query-sf').remove();
                tableBody.prepend('<tr class="search-query-sf"><td colspan="6"><strong>Searching for: "'
                    + $(that).val()
                    + '"</strong></td></tr>');
            }
            else
            {
                $('.search-query-sf').remove();
            }

            if( rowText.indexOf( inputText ) == -1 )
            {
                //hide rows
                tableRowsClass.eq(i).hide();
                
            }
            else
            {
                $('.search-sf').remove();
                tableRowsClass.eq(i).show();
            }
        });
        //all tr elements are hidden
        if(tableRowsClass.children(':visible').length == 0)
        {
            tableBody.append('<tr class="search-sf"><td class="text-muted" colspan="6">No entries found.</td></tr>');
        }
    });
});
</script>
<script type="text/javascript">
		$(document).ready(function() {
			$('#createBorrowerForm').validate({
				ignore: ':not(select:hidden, input:visible, textarea:visible)',
				rules : {
					name : {
						minlength : 1,
						maxlength : 100,
						required : true
					}
				},
				highlight : function(element) {
					$(element).closest('.form-group').removeClass('has-success');
					$(element).closest('.form-group').addClass('has-error');
				},
				success : function(element) {
					element.closest('.form-group').removeClass('has-error');
					element.closest('.form-group').addClass('has-success');
				}
			});
		});
</script>
</body>
</html>