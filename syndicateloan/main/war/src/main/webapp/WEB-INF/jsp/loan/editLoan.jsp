<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
<title>Danskebank - Syndicateloan</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="<%=request.getContextPath()%>/style/bootstrap.css"
	type="text/css" rel="stylesheet" />
<link href="<%=request.getContextPath()%>/style/bootstrap-select.css"
	type="text/css" rel="stylesheet" />
<link href="<%=request.getContextPath()%>/style/validation.css"
	type="text/css" rel="stylesheet" />
<style type="text/css"></style>

</head>
<body>

	<div class="container">
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand"
					href="<%=request.getContextPath()%>/index.html">Syndicateloan</a>
			</div>
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="<%=request.getContextPath()%>/index.html">Home</a></li>
					<li><a href="<%=request.getContextPath()%>/search.html">Search</a></li>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown">Create<b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a
								href="<%=request.getContextPath()%>/createBorrower.html">Create
									borrower</a></li>
							<li class="divider"></li>
							<li><a href="<%=request.getContextPath()%>/editLoan/0.html">Create
									loan</a></li>
						</ul></li>
						<li><a href="<%=request.getContextPath()%>/setup.html">Setup</a></li>
				</ul>
			</div>
		</nav>
		<form:form commandName="editLoanBean" name="editLoanForm" id="editLoanForm">
			<div class="row">
				<h3>Borrower</h3>
				<hr>
				<div class="panel-body">
					<div class="row">
						<div class="form-group">
							<form:select class="selectpicker" title="Borrower" data-msg-required="Select borrower!" data-rule-required="true"
								data-width="150px" path="borrowerId" name="borrower" id="borrower">
								<option data-hidden="true" value=""></option>
								<form:options items="${borrowers}" itemValue="id"
									itemLabel="name" />
							</form:select>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<h3>Transaction</h3>
				<hr>
				<div class="panel-body">
					<div class="row">
						<div class="form-group">
							<label class='control-label'>Amount</label>
							<form:input path="amount" type="text" class="form-control"
								name="amount" id="amount" />
						</div>
						<div class="form-group">
							<label class='control-label'>Maturity</label>
							<form:input path="maturity" type="text" class="form-control" />
						</div>
						<div class="form-group">
							<label class='control-label'>Extension options</label>
							<form:input path="extensionOptions" type="text"
								class="form-control" />
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<form:select class="selectpicker" title="Type" data-width="150px"
								path="type" data-msg-required="Select type!" data-rule-required="true" >
								<option data-hidden="true"></option>
								<form:options items="${types}" itemValue="Type" itemLabel="type" />
							</form:select>
							<form:select class="selectpicker" title="Facility"
								data-width="150px" path="facility" data-msg-required="Select facility!" data-rule-required="true">
								<option data-hidden="true"></option>
								<form:options items="${facilities}" itemValue="Facility"
									itemLabel="facility" />
							</form:select>
							<form:select class="selectpicker" title="Status"
								data-width="150px" path="status" data-msg-required="Select status!" data-rule-required="true">
								<option data-hidden="true"></option>
								<form:options items="${statuses}" itemValue="Status"
									itemLabel="status" />
							</form:select>
							<form:select class="selectpicker" title="Year" data-width="150px"
								path="year" data-msg-required="Select year!" data-rule-required="true">
								<option data-hidden="true"></option>
								<form:options items="${years}" />
							</form:select>
							<form:select class="selectpicker" title="Month"
								data-width="150px" path="month" data-msg-required="Select month!" data-rule-required="true">
								<option data-hidden="true"></option>
								<form:options items="${months}" />
							</form:select>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<h3>Price</h3>
				<hr>
				<div class="panel-body">
					<div class="row">
						<div class="form-group">
							<label class='control-label'>Up Front Fee</label>
							<form:input path="upFrontFee" type="text" class="form-control" />
						</div>
						<div class="form-group">
							<label class='control-label'>Margin</label>
							<form:input path="margin" type="text" class="form-control" />
						</div>
						<div class="form-group">
							<label class='control-label'>Utilization Fee</label>
							<form:input path="utilizationFee" type="text"
								class="form-control" />
						</div>
						<div class="form-group">
							<label class='control-label'>Fully Drawn Price</label>
							<form:input path="fullyDrawnPrice" type="text"
								class="form-control" />
						</div>
						<div class="form-group">
							<label class='control-label'>Comment</label><br>
							<form:textarea path="comment" class="form-control" type="text"/>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group">
					<button type="submit" class="btn btn-primary">Save</button>
					<a href="<%=request.getContextPath()%>/createBorrower.html"
						class="btn btn-primary">Cancel</a>
				</div>
			</div>
		</form:form>
		<form:form method="delete">
			<input type="submit" class="btn btn-primary" style="float: right"
				value="Delete" />
		</form:form>
		
	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="<%=request.getContextPath()%>/js/jquery.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="<%=request.getContextPath()%>/js/bootstrap.js"></script>
	<script src="<%=request.getContextPath()%>/js/bootstrap-select.js"></script>
	<!-- Validation -->
	<script src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>

	<script type="text/javascript">
		$(document).ready(function(e) {
			$('.selectpicker').selectpicker({
				style : 'btn-primary',
				size : 4
			});
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#editLoanForm').validate({
				ignore: ':not(select:hidden, input:visible, textarea:visible)',
				rules : {
					amount : {
						min : 0,
						max : 1000,
						required : true
					},
					maturity : {
						min : 0,
						max : 5,
						required : true
					},
					extensionOptions : {
						min : 0,
						max : 2,
						required : true
					},
					upFrontFee : {
						min : 0,
						max : 1000,
						required : true
					},
					margin : {
						min : 0,
						max : 1000,
						required : true
					},
					utilizationFee : {
						min : 0,
						max : 1000,
						required : true
					},
					fullyDrawnPrice : {
						min : 0,
						max : 2000,
						required : true
					},
					comment : {
						minlength : 0,
						maxlength : 200,
						required : true
					},
				},
				highlight : function(element) {
					$(element).closest('.form-group').removeClass('has-success');
					$(element).closest('.form-group').addClass('has-error');
				},
				success : function(element) {
					element.closest('.form-group').removeClass('has-error');
					element.closest('.form-group').addClass('has-success');
				}
			});
		});
	</script>
</body>
</html>