<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
<title>Danskebank - Syndicateloan</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="<%=request.getContextPath()%>/style/bootstrap.css"
	type="text/css" rel="stylesheet" />
<link href="<%=request.getContextPath()%>/style/bootstrap-select.css"
	type="text/css" rel="stylesheet" />
<link href="<%=request.getContextPath()%>/style/index.css"
	type="text/css" rel="stylesheet" />
<link href="<%=request.getContextPath()%>/style/slider.css"
	type="text/css" rel="stylesheet" />
<link href="<%=request.getContextPath()%>/style/table.css"
	type="text/css" rel="stylesheet" />
<style type="text/css"></style>

</head>
<body>

	<div class="container">
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand"
					href="<%=request.getContextPath()%>/index.html">Syndicateloan</a>
			</div>
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="<%=request.getContextPath()%>/index.html">Home</a></li>
					<li><a href="<%=request.getContextPath()%>/search.html">Search</a></li>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown">Create<b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a
								href="<%=request.getContextPath()%>/createBorrower.html">Create
									borrower</a></li>
							<li class="divider"></li>
							<li><a href="<%=request.getContextPath()%>/editLoan/0.html">Create
									loan</a></li>
						</ul></li>
					<li><a href="<%=request.getContextPath()%>/setup.html">Setup</a></li>
				</ul>
			</div>
		</nav>
		<form:form commandName="searchBean">
			<div class="row" id="borrower">
				<h3>Borrower</h3>
				<hr>
				<div class="panel-body">
					<div class="row">
						<div class="form-group">
							<label class='control-label'>Name</label>
							<form:input path="name" type="text" class="form-control" />
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<form:select class="selectpicker" title="Nationality"
								data-width="150px" path="nationality">
								<option data-hidden="true"></option>
								<form:options items="${nationalities}" itemValue="Nationality"
									itemLabel="nationality" />
							</form:select>
							<form:select class="selectpicker" title="Sector"
								data-width="150px" path="sector">
								<option data-hidden="true"></option>
								<form:options items="${sectors}" itemValue="Sector"
									itemLabel="sector" />
							</form:select>
							<form:select class="selectpicker" title="Debt category"
								data-width="150px" path="debtCategory">
								<option data-hidden="true"></option>
								<form:options items="${debtCategories}" itemValue="DebtCategory"
									itemLabel="debtCategory" />
							</form:select>
							<form:select class="selectpicker" title="DB rating"
								data-width="150px" path="dbRating">
								<option data-hidden="true"></option>
								<form:options items="${dbRatings}" itemValue="DbRating"
									itemLabel="dbRating" />
							</form:select>
							<form:select class="selectpicker" title="External rating"
								data-width="150px" path="externalRating">
								<option data-hidden="true"></option>
								<form:options items="${externalRatings}"
									itemValue="ExternalRating" itemLabel="externalRating" />
							</form:select>
						</div>
					</div>
				</div>
			</div>
			<div class="row" id="transaction">
				<h3>Transaction</h3>
				<hr>
				<div class="panel-body">
					<div class="row">
						<!--  <div class="form-group">
							<label class='control-label'>Amount</label>
							<form:input path="amount" type="text" class="form-control" />
						</div>-->
						<div class="form-group">
							<label class='control-label'>Amount</label>
							<form:input path="amount" type="text" class="slider"
								data-slider-min="0" data-slider-max="1000"
								data-slider-value="[0, 1000]" style="width: 1170px;" />
						</div>
						<div class="form-group">
							<label class='control-label'>Maturity</label>
							<form:input path="maturity" type="text" class="slider"
								data-slider-min="0" data-slider-max="5"
								data-slider-value="[0, 5]" style="width: 1170px;" />
						</div>
						<div class="form-group">
							<label class='control-label'>Extension options</label>
							<form:input path="extensionOptions" type="text" class="slider"
								data-slider-min="0" data-slider-max="2"
								data-slider-value="[0, 2]" style="width: 1170px;" />
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<form:select class="selectpicker" title="Year" data-width="150px"
								path="year">
								<option data-hidden="true"></option>
								<form:options items="${years}" />
							</form:select>
							<form:select class="selectpicker" title="Month"
								data-width="150px" path="month">
								<option data-hidden="true"></option>
								<form:options items="${months}" />
							</form:select>

							<form:select class="selectpicker" title="Type" data-width="150px"
								path="type">
								<option data-hidden="true"></option>
								<form:options items="${types}" itemValue="Type" itemLabel="type" />
							</form:select>
							<form:select class="selectpicker" title="Facility"
								data-width="150px" path="facility">
								<option data-hidden="true"></option>
								<form:options items="${facilities}" itemValue="Facility"
									itemLabel="facility" />
							</form:select>
							<form:select class="selectpicker" title="Status"
								data-width="150px" path="status">
								<option data-hidden="true"></option>
								<form:options items="${statuses}" itemValue="Status"
									itemLabel="status" />
							</form:select>

						</div>
					</div>
				</div>
			</div>
			<div class="row" id="price">
				<h3>Price</h3>
				<hr>
				<div class="panel-body">
					<div class="row">
						<div class="form-group">
							<label class='control-label'>Up Front Fee</label>
							<form:input path="upFrontFee" type="text" class="slider"
								data-slider-min="0" data-slider-max="1000"
								data-slider-value="[0, 1000]" style="width: 1170px;" />
						</div>
						<div class="form-group">
							<label class='control-label'>Margin</label>
							<form:input path="margin" type="text" class="slider"
								data-slider-min="0" data-slider-max="1000"
								data-slider-value="[0, 1000]" style="width: 1170px;" />
						</div>
						<div class="form-group">
							<label class='control-label'>Utilization Fee</label>
							<form:input path="utilizationFee" type="text" class="slider"
								data-slider-min="0" data-slider-max="1000"
								data-slider-value="[0, 1000]" style="width: 1170px;" />
						</div>
						<div class="form-group">
							<label class='control-label'>Fully Drawn Price</label>
							<form:input path="fullyDrawnPrice" type="text" class="slider"
								data-slider-min="0" data-slider-max="2000"
								data-slider-value="[0, 2000]" style="width: 1170px;" />
						</div>

						<form:select class="selectpicker" title="Sort field"
							data-width="150px" path="sortField">
							<option data-hidden="true"></option>
							<form:options items="${sortFields}" />
						</form:select>
					</div>
				</div>
				<button id="search" type="submit" class="btn btn-primary">
					<i class="glyphicon glyphicon-search"></i> Search
				</button>
			</div>
		</form:form>
		<div class="row" id="result">
			<h3>Search result</h3>
			<hr>

			<c:choose>
				<c:when test="${empty loans }">
					<h4>Could not find any loans with the selected parameters, try
						again</h4>
				</c:when>
				<c:otherwise>


					<table class="table table-condensed">
						<thead>
							<tr>
								<th>Year</th>
								<th>Month</th>
								<th>Debt category</th>
								<th>Nationality</th>
								<th>Borrower</th>
								<th>DB rating</th>
								<th>ext (exp) rating</th>
								<th>Type</th>
								<th>Facility</th>
								<th>Status</th>
								<th bgcolor="#d4d4d4">Amount (EURm)</th>
								<th>Maturity</th>
								<th># of extn</th>
								<th bgcolor="#d4d4d4">Up front fee</th>
								<th bgcolor="#d4d4d4">Margin</th>
								<th bgcolor="#d4d4d4">Util. Fee Full</th>
								<th bgcolor="#d4d4d4">Fully Drawn Price</th>
								<th>Sector</th>
								<th>Comment</th>
								<th class="text-center">Action</th>
							</tr>
						</thead>
						<c:forEach items="${loans}" var="loan">
							<tr>
								<td>${loan.transaction.year}</td>
								<td>${loan.transaction.month}</td>
								<td>${loan.borrower.debtCategory}</td>
								<td>${loan.borrower.nationality}</td>
								<td>${loan.borrower.name}</td>
								<td>${loan.borrower.dbRating}</td>
								<td>${loan.borrower.externalRating}</td>
								<td>${loan.transaction.type}</td>
								<td>${loan.transaction.facility}</td>
								<td>${loan.transaction.status}</td>
								<td bgcolor="#d4d4d4"><b>${loan.transaction.amount}</b></td>
								<td>${loan.transaction.maturity}</td>
								<td>${loan.transaction.extensionOptions}</td>
								<td bgcolor="#d4d4d4"><b>${loan.price.upFrontFee}</b></td>
								<td bgcolor="#d4d4d4"><b>${loan.price.margin}</b></td>
								<td bgcolor="#d4d4d4"><b>${loan.price.utilizationFee}</b></td>
								<td bgcolor="#d4d4d4"><b>${loan.price.fullyDrawnPrice}</b></td>
								<td>${loan.borrower.sector}</td>
								<c:choose>
									<c:when test="${loan.price.comments == ''}">
									<td>No</td>
									</c:when>
									<c:otherwise>
									<td>Yes</td>
									</c:otherwise>
								</c:choose>
								<td class="text-center"><a class='btn btn-primary btn-xs'
									href="<%= request.getContextPath() %>/editLoan/${loan.transaction.id}.html"><span
										class="glyphicon glyphicon-edit"></span> Edit</a></td>
							</tr>
						</c:forEach>
					</table>
				</c:otherwise>
			</c:choose>
		</div>
	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="<%=request.getContextPath()%>/js/jquery.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="<%=request.getContextPath()%>/js/bootstrap.js"></script>
	<script src="<%=request.getContextPath()%>/js/bootstrap-select.js"></script>
	<script src="<%=request.getContextPath()%>/js/bootstrap-slider.js"></script>
	<script type="text/javascript">
		$(document).ready(function(e) {
			$('.selectpicker').selectpicker({
				style : 'btn-primary',
				size : 4
			});
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function(e) {
			$('.slider').slider({
				step : 1,
				tooltip : 'show',
				handle : 'round'
			});
		});
	</script>
</body>
</html>